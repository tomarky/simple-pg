// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// Form1.java  クラス
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// インポート
//import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
//import java.io.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class Form1 extends Form {
	private TextField    tf1;
	private Command      cmds;
	
	//コンストラクタ
	Form1(CommandListener c,String title) {
		super(title);
		
		tf1=new TextField("",null,11,TextField.NUMERIC);
		append(tf1);
		
		cmds=new Command("OK",  Command.OK,0);
		addCommand(cmds);
		
		setCommandListener(c);
	}
	
	public int data() {
		if (tf1.size()==0)
			return 0;
		else {
			long v=Long.parseLong(tf1.getString());
			if (v<(long)Integer.MIN_VALUE) v=(long)Integer.MIN_VALUE;
			if (v>(long)Integer.MAX_VALUE) v=(long)Integer.MAX_VALUE;
			return (int)v;
		}
	}


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //Form1の終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
