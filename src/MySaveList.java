// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MySaveList.java  クラス
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// インポート
//import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
//import java.io.*;
import java.util.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class MySaveList {

	private SaveList savelist=null;
	public int       size=0;
	public int       usedsize=0;

	//コンストラクタ
	MySaveList() {
	}

	public void reset() {
		int c=count();
		while (savelist!=null)
			delete(0);
		size=0;
		usedsize=0;
	}

	public void add(int recordId, String name, long datetime, int datasize) {
		if (savelist!=null)
			savelist.add(recordId, name, datetime, datasize);
		else
			savelist=new SaveList(recordId, name, datetime, datasize);
	}
	
	public void delete(TextField tf1) {
		if (savelist!=null)
			savelist=savelist.delete(tf1);
	}

	public void delete(int n) {
		if (savelist!=null)
			savelist=savelist.delete(n);
	}
	
	public int count() {
		if (savelist!=null)
			return savelist.count();
		else
			return 0;
	}
	
	public String Name(TextField tf1) {
		if (savelist!=null)
			return savelist.Name(tf1);
		else
			return null;
	}
	
	public int RecordId(TextField tf1) {
		if (savelist!=null)
			return savelist.RecordId(tf1);
		else
			return -1;
	}
	
	public TextField getTextField(int n) {
		if (savelist!=null)
			return savelist.getTextField(n);
		else
			return null;
	}

	class SaveList {
		public String     name=null;
		//public long       datetime=0L;
		//public int        datasize=0;
		public int        recordId=0;
		public SaveList   next=null;
		public TextField  textfield=null;
		
		SaveList(int rid, String nm, long dt, int ds) {
			recordId=rid;
			name=new String(nm);
			//datetime=dt;
			//datasize=ds;
			String d=(new Date(dt)).toString();
			textfield=new TextField(nm+" ( "+Integer.toString(ds)+" B )",
			                        d,d.length(),TextField.UNEDITABLE);
			textfield.setLayout(Item.LAYOUT_NEWLINE_BEFORE);
		}
		
		public void add(int rid, String nm, long dt, int ds) {
			if (next!=null) {
				next.add(rid,nm,dt,ds);
			} else {
				next=new SaveList(rid,nm,dt,ds);
			}
		}
		
		public SaveList delete(TextField tf1) {
			SaveList sl=null;
			if (tf1==textfield) {
				sl=next;
				next=null;
				textfield=null;
				return sl;
			} else {
				if (next!=null) 
					next=next.delete(tf1);
				return this;
			}
		}
		
		public SaveList delete(int n) {
			SaveList sl=null;
			if (n==0) {
				sl=next;
				next=null;
				textfield=null;
				return sl;
			} else {
				if (next!=null) 
					next=next.delete(n-1);
				return this;
			}
		}
		
		public int count() {
			if (next!=null)
				return 1+next.count();
			else
				return 1;
		}
		
		public String Name(TextField tf1) {
			if (tf1==textfield)	
				return name;
			else if (next!=null)
				return next.Name(tf1);
			else
				return null;
		}
		
		public int RecordId(TextField tf1) {
			if (tf1==textfield)	
				return recordId;
			else if (next!=null)
				return next.RecordId(tf1);
			else
				return -1;
		}
		
		public TextField getTextField(int n) {
			if (n==0)
				return textfield;
			else if (next!=null)
				return next.getTextField(n-1);
			else
				return null;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //MySaveListの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
