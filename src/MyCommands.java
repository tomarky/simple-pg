// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MyCommands.java  クラス
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// インポート
//import javax.microedition.lcdui.game.*;
//import javax.microedition.lcdui.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
import java.io.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class MyCommands {
	
	public static final int   CMD_VERSION = 1;
	public static final int   MES_SIZE    = 7;
	public static final int   MES_LENGTH  =20;
	
	private MyCommand   mcFirst=null;

	public static String[]     mes=new String[MES_SIZE];

	//コンストラクタ
	MyCommands() {
		int i;
		for (i=0;i<mes.length;i++)
			mes[i]=null;
	}
	
	public byte[] toBytes() {
		ByteArrayOutputStream out=new ByteArrayOutputStream(4);
		DataOutputStream      dos=new DataOutputStream(out);
		MyCommand             mc=null;
		
		int    c=count();
		int    i,n;

		byte[] bData=null;
		
		try {
			byte[] b=null;
			dos.writeInt(CMD_VERSION); //バージョン

			dos.writeInt(MES_SIZE);
			for (i=0;i<mes.length;i++) { //文字列保存
				if (mes[i]==null) {
					dos.writeInt(-1);
				} else if (mes[i].length()==0) {
					dos.writeInt(0);
				} else {
					b=mes[i].getBytes();
					n=b.length;
					dos.writeInt(n);
					dos.write(b,0,n);
				}
			}

			dos.writeInt(c);           //コマンド数
			mc=mcFirst;
			while (mc!=null) {
				b=mc.toBytes();
				if (b==null) return null;
				dos.write(b,0,b.length);
				mc=mc.next;
			}
			bData=out.toByteArray();
			dos.close();

		} catch(Exception e) {
			System.out.println("MyCommands.toBytes::"+e.toString());
			try {
				dos.close();
			} catch (Exception e2) {
				System.out.println("MyCommands.toBytes(close)::"+e2.toString());
			}
			return null;
		}
		return bData;
	}
	
	public void AllClear() {
		while (mcFirst!=null) {
			mcFirst=mcFirst.del(0,false);
		}
		int i;
		for (i=0;i<MES_SIZE;i++)
			mes[i]=null;
	}
	
	public void ReNumber() {
		if (mcFirst!=null)
			mcFirst.ReNumber(1);
	}
	
	public void setBack() {
		if (mcFirst!=null)
			mcFirst.setBack(null);
	}
	
	public void addnew(MyCommand mc) {
		if (mcFirst==null)
			mcFirst=mc;
		else {
			mcFirst.addnew(mc);
		}
	}
	
	public void revise(MyCommand mc) {
		if (mcFirst!=null) {
			mcFirst=mcFirst.revise(mc);
		}
	}

	public void insert(MyCommand mc) {
		if (mcFirst!=null) 
			mcFirst=mcFirst.insert(mc);
		else
			mcFirst=mc;
	}

	public void delNum(int num, boolean renum) {
		if (mcFirst!=null)
			mcFirst=mcFirst.delNum(num,renum);
	}
	
	public void del(int cnt, boolean renum) {
		if (mcFirst!=null)
			mcFirst=mcFirst.del(cnt,renum);
	}
	
	public int count() {
		if (mcFirst!=null)
			return mcFirst.count();
		else
			return 0;
	}
	
	public int NumCount(int num) {
		if (mcFirst!=null)
			return mcFirst.NumCount(num);
		else
			return 0;
	}
	
	public MyCommand Data(int cnt) {
		if (mcFirst!=null)
			return mcFirst.Data(cnt);
		else
			return null;
	}
	
	public MyCommand DataNum(int num) {
		if (mcFirst!=null)
			return mcFirst.DataNum(num);
		else
			return null;
	}
	
	
	public int LastNum() {
		if (mcFirst==null)
			return 0;
		else
			return mcFirst.LastNum();
	}
	
	public int NextNum() {
		return this.LastNum()+1;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //MyCommandsの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
