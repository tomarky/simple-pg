// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MyProgram.java  クラス
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// インポート
//import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
//import java.io.*;
import java.util.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class MyProgram {

	private final int      ERRNUM_OVERFLOW    =   1,
	                       ERRNUM_DIVIDEZERO  =   2,
	                       ERRNUM_NOLABEL     =   3,
	                       ERRNUM_NORETURN    =   4,
	                       ERRNUM_WRONGTIME   =   6,
	                       ERRNUM_NOSTACK     =   7,
	                       ERRNUM_WRONGPOWER  =   8,
	                       ERRNUM_WRONGSEED   =   9,
	                       ERRNUM_WRONGJUMP   =  10,
	                       ERRNUM_JUMPSELF    =  11,
	                       ERRNUM_NONEXT      =  12,
	                       ERRNUM_WRONGVALUE  =  13,
	                       ERRNUM_WRONGINDEX  =  14,
	                       ERRNUM_REQSTOP     =  99,
	                       ERRNUM_UNKNOWN     = 999;

	private final int      RESULT_NEXT        =   1,
	                       RESULT_STAY        =   0,
	                       RESULT_END         =  -1,
	                       RESULT_NOCOMMAND   =  -2,
	                       RESULT_ERROR       =  -3,
	                       RESULT_REQSTOP     = -99,
	                       RESULT_REQFINISH   =-999;

	private final int      CMDNUM_STOP        =  14;
	
	private final int      ARRAY_SIZE         =  30;

	private Random         rndm=new Random();      //乱数

	private Canvas1        canvas1=null;
	private MyText         mytext=null;
	private MyCommands     mycommands=null;

	private MyStack2       valueStack=new MyStack2(); //PUSH,POP用
	private MyStack        pgcStack=new MyStack();    //GOSUB,RETURN用
	private int[]          iVar=null;       //変数
	private MyCommand[]    mcLabel=null;    //ラベル
	private MyCommand      nowCommand=null; //実行するコマンド
	private int            iErr=0;          //エラー番号
	private boolean        inputflag=false; //inputフラグ
	private int            inputdata=0;     //inputされたデータ
	
	private boolean        finishFlag=false;
	
	private int            cmd=-999, keyEvent=-999, keyNum=0;
	
	private Graphics       gg=null;
	private int            w=240;
	private int            h=268;
	private int            pX=0, pY=0, iR=255, iG=255, iB=255,
	                       arcStart=0, arcAngle=360;
	
	private int[]          myArray=new int[ARRAY_SIZE];
	
	private boolean        flushflag=true;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	//コンストラクタ
	MyProgram(Canvas1 c1, MyText mt, MyCommands mcs, Graphics g) {
		canvas1=c1;
		mytext=mt;
		mycommands=mcs;
		gg=g;
		
		iVar=new int[MyCommand.VAR_NAME.length];
		mcLabel=new MyCommand[MyCommand.LBL_NAME.length];
		
		int i;
		for (i=0;i<ARRAY_SIZE;i++)
			myArray[i]=0;
		
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public void commandAction(int c) {
		cmd=c;
	}
	
	public void keyPressed(int keyCode) {
		keyEvent=keyCode;
		int i;
		for (i=0;i<MyCommand.KEY_CODES.length;i++) 
			if (MyCommand.KEY_CODES[i]==keyCode) {
				keyNum=i;
				break;
			}
	}
	
	public void keyReleased(int keyCode) {
		if (keyEvent==keyCode) {
			keyEvent=-999;
			keyNum=0;
		}
	}
	
	public void input(int data) {
		inputdata=data;
		inputflag=false;
	}
	
	public void finish() {
		finishFlag=true;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public void resetRun() {
		MyCommand mc;
		int i,c,n;
		iErr=0;
		cmd=-999;
		keyEvent=-999;
		keyNum=0;
		inputflag=false;
		flushflag=true;
		pgcStack.reset();
		//valueStack.reset();
		for (i=0;i<26;i++)
			mcLabel[i]=null;
		mycommands.setBack();
		c=mycommands.count();
		for (i=0;i<c;i++) {
			mc=mycommands.Data(i);
			if (mc.iData[0]==MyCommand.CMDNUM_LABEL) {
				n=mc.iData[1];
				mcLabel[n]=mc;
			}
		}
		nowCommand=mycommands.Data(0);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public void doRun() {
		
		int r=0;
		
		while (r>=0) {
			r=doCommand(nowCommand);
			if (r>0) {
				nowCommand=nowCommand.next;
			}
			if (cmd==CMDNUM_STOP) {
				r=RESULT_REQSTOP;
				iErr=ERRNUM_REQSTOP;
				cmd=-999;
			}
			if (finishFlag) {
				//r=RESULT_FINISH;
				return;
			}
		}
		
		flushflag=true;
		cmd=-999;
		keyEvent=-999;
		keyNum=0;
		inputflag=false;
		
		if (iErr==0) {
			mytext.print("Finish "+r);
		} else {
			mytext.print("Error "+iErr);
			if (nowCommand!=null)
				mytext.print(">>> "+nowCommand.strData());
			String mes;
			switch (iErr) {
			case ERRNUM_OVERFLOW:
				mes="Overflow";
				break;
			case ERRNUM_DIVIDEZERO:
				mes="Zero Divide";
				break;
			case ERRNUM_NOLABEL:
				mes="Label is not Found";
				break;
			case ERRNUM_NORETURN:
				mes="No Exist To Return";
				break;
			case ERRNUM_WRONGTIME:
				mes="Wrong Time";
				break;
			case ERRNUM_NOSTACK:
				mes="No Stack Data";
				break;
			case ERRNUM_WRONGPOWER:
				mes="Wrong Power Number";
				break;
			case ERRNUM_WRONGSEED:
				mes="Wrong Seed Number";
				break;
			case ERRNUM_WRONGJUMP:
				mes="Wrong Jump Number";
				break;
			case ERRNUM_JUMPSELF:
				mes="Jump to Self";
				break;
			case ERRNUM_NONEXT:
				mes="Next Command is not found";
				break;
			case ERRNUM_WRONGVALUE:
				mes="Wrong Value";
				break;
			case ERRNUM_WRONGINDEX:
				mes="Wrong Index";
				break;
			case ERRNUM_REQSTOP:
				mes="Stop";
				break;
			default:
				mes="Unknown";
				break;
			}
			mytext.print(mes);
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public int selValue(MyCommand mc, int p) {
		if (mc.iData[p]==0)
			return iVar[mc.iData[p+1]];
		else
			return mc.iData[p+1];
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public MyCommand getJumpto(MyCommand mc, int p) {
		MyCommand mc2=null;
		switch (mc.iData[p]) {
		case 0: case 3: //LABEL 
			mc2=mcLabel[mc.iData[p+1]];
			if (mc2==null)
				iErr=ERRNUM_NOLABEL;
			break;
		case 1: case 4: //ABSOLUTE NUMBER 
			if (mc.iData[p+1]<1) {
				iErr=ERRNUM_WRONGJUMP;
				return null;
			}
			if (mc.iNum>mc.iData[p+1]) {
				mc2=mc.DataNumRv(mc.iData[p+1]);
			} else if (mc.iNum<mc.iData[p+1]) {
				mc2=mc.DataNum(mc.iData[p+1]);
			} else {
				if ((mc.iData[0]==MyCommand.CMDNUM_KEYSTAT) 
				         && (mc.iData[p]==1)) {
					mc2=mc;
				} else {
					iErr=ERRNUM_JUMPSELF;
					return null;
				}
			}
			if (mc2==null)
				iErr=ERRNUM_WRONGJUMP;
			break;
		case 2: case 5: //RELATIVE NUBMER 
			if (mc.iData[p+1]<0) {
				mc2=mc.DataRv(mc.iData[p+1]);
			} else if (mc.iData[p+1]>0) {
				mc2=mc.Data(mc.iData[p+1]);
			} else {
				if ((mc.iData[0]==MyCommand.CMDNUM_KEYSTAT) 
				         &&(mc.iData[p]==2)) {
					mc2=mc;
				} else {
					iErr=ERRNUM_JUMPSELF;
					return null;
				}
			}
			if (mc2==null)
				iErr=ERRNUM_WRONGJUMP;
			break;
		case 6: //RETURN
			mc2=pgcStack.pop();
			if (mc2==null)
				iErr=ERRNUM_NORETURN;
			break;
		default: //???
			iErr=ERRNUM_UNKNOWN;
			break;
		}
		return mc2;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public int doCommand(MyCommand mc) {
		MyCommand mc2=null;
		int v=0,n,i,r,g,b;
		long vl=0L, vl2=0L;
		double d;
		
		if (mc==null) {
			iErr=ERRNUM_NONEXT;
			return RESULT_NOCOMMAND;
		}

		//System.out.println(mc.strData()); //デバッグ用

		switch (mc.iData[0]) {
		case MyCommand.CMDNUM_END:
			return RESULT_END;
		case MyCommand.CMDNUM_LET: //LET
			iVar[mc.iData[1]]=selValue(mc,2);
			break;
		case MyCommand.CMDNUM_ADD: //ADD
			vl=(long)selValue(mc,2)+(long)selValue(mc,4);
			if ((vl<(long)Integer.MIN_VALUE)||(vl>(long)Integer.MAX_VALUE)) {
				iErr=ERRNUM_OVERFLOW; return RESULT_ERROR;
			}
			iVar[mc.iData[1]]=(int)vl;
			break;
		case MyCommand.CMDNUM_SUB: //SUB
			vl=(long)selValue(mc,2)-(long)selValue(mc,4);
			if ((vl<(long)Integer.MIN_VALUE)||(vl>(long)Integer.MAX_VALUE)) {
				iErr=ERRNUM_OVERFLOW; return RESULT_ERROR;
			} 
			iVar[mc.iData[1]]=(int)vl;
			break;
		case MyCommand.CMDNUM_MUL: //MUL
			vl=(long)selValue(mc,2)*(long)selValue(mc,4);
			if ((vl<(long)Integer.MIN_VALUE)||(vl>(long)Integer.MAX_VALUE)) {
				iErr=ERRNUM_OVERFLOW; return RESULT_ERROR;
			} 
			iVar[mc.iData[1]]=(int)vl;
			break;
		case MyCommand.CMDNUM_DIV: //DIV
			v=selValue(mc,4);
			if (v==0) {
				iErr=ERRNUM_DIVIDEZERO; return RESULT_ERROR;
			}
			vl=(long)selValue(mc,2)/(long)v;
			if ((vl<(long)Integer.MIN_VALUE)||(vl>(long)Integer.MAX_VALUE)) {
				iErr=ERRNUM_OVERFLOW; return RESULT_ERROR;
			} 
			iVar[mc.iData[1]]=(int)vl;
			break;
		case MyCommand.CMDNUM_REM: //REM
			v=selValue(mc,4);
			if (v==0) {
				iErr=ERRNUM_DIVIDEZERO; return RESULT_ERROR;
			}
			vl=(long)selValue(mc,2)%(long)v;
			if ((vl<(long)Integer.MIN_VALUE)||(vl>(long)Integer.MAX_VALUE)) {
				iErr=ERRNUM_OVERFLOW; return RESULT_ERROR;
			} 
			iVar[mc.iData[1]]=(int)vl;
			break;
		case MyCommand.CMDNUM_RAISE: //RAISE
			v=selValue(mc,4);
			if (v<0) {
				iErr=ERRNUM_WRONGPOWER; return RESULT_ERROR;
			}
			vl=1L; vl2=(long)selValue(mc,2);
			for (i=0;i<v;i++) {
				vl*=vl2;
				if ((vl<(long)Integer.MIN_VALUE)||(vl>(long)Integer.MAX_VALUE)) {
					iErr=ERRNUM_OVERFLOW; return RESULT_ERROR;
				}
			}
			iVar[mc.iData[1]]=(int)vl;
			break;
		case MyCommand.CMDNUM_CMPE: //CMPE
			if (iVar[mc.iData[1]]==selValue(mc,2)) {
				mc2=getJumpto(mc,4);
				if (mc2!=null) {
					switch (mc.iData[4]) {
					case 3: case 4: case 5:
						pgcStack.push(nowCommand); break;
					}
					nowCommand=mc2;
					if (mc.iData[4]!=6)
						return RESULT_STAY;
				} else {
					return RESULT_ERROR;
				}
			}
			break;
		case MyCommand.CMDNUM_CMPL: //CMPL
			if (iVar[mc.iData[1]]>selValue(mc,2)) {
				mc2=getJumpto(mc,4);
				if (mc2!=null) {
					switch (mc.iData[4]) {
					case 3: case 4: case 5:
						pgcStack.push(nowCommand); break;
					}
					nowCommand=mc2;
					if (mc.iData[4]!=6)
						return RESULT_STAY;
				} else {
					return RESULT_ERROR;
				}
			}
			break;
		case MyCommand.CMDNUM_CMPLE: //CMPLE
			if (iVar[mc.iData[1]]>=selValue(mc,2)) {
				mc2=getJumpto(mc,4);
				if (mc2!=null) {
					switch (mc.iData[4]) {
					case 3: case 4: case 5:
						pgcStack.push(nowCommand); break;
					}
					nowCommand=mc2;
					if (mc.iData[4]!=6)
						return RESULT_STAY;
				} else {
					return RESULT_ERROR;
				}
			}
			break;
		case MyCommand.CMDNUM_CMPS: //CMPS
			if (iVar[mc.iData[1]]<selValue(mc,2)) {
				mc2=getJumpto(mc,4);
				if (mc2!=null) {
					switch (mc.iData[4]) {
					case 3: case 4: case 5:
						pgcStack.push(nowCommand); break;
					}
					nowCommand=mc2;
					if (mc.iData[4]!=6)
						return RESULT_STAY;
				} else {
					return RESULT_ERROR;
				}
			}
			break;
		case MyCommand.CMDNUM_CMPSE: //CMPSE
			if (iVar[mc.iData[1]]<=selValue(mc,2)) {
				mc2=getJumpto(mc,4);
				if (mc2!=null) {
					switch (mc.iData[4]) {
					case 3: case 4: case 5:
						pgcStack.push(nowCommand); break;
					}
					nowCommand=mc2;
					if (mc.iData[4]!=6)
						return RESULT_STAY;
				} else {
					return RESULT_ERROR;
				}
			}
			break;
		case MyCommand.CMDNUM_CMPNE: //CMPNE
			if (iVar[mc.iData[1]]!=selValue(mc,2)) {
				mc2=getJumpto(mc,4);
				if (mc2!=null) {
					switch (mc.iData[4]) {
					case 3: case 4: case 5:
						pgcStack.push(nowCommand); break;
					}
					nowCommand=mc2;
					if (mc.iData[4]!=6)
						return RESULT_STAY;
				} else {
					return RESULT_ERROR;
				}
			}
			break;
		case MyCommand.CMDNUM_GOTO: //GOTO
			mc2=getJumpto(mc,1);
			if (mc2!=null) {
				nowCommand=mc2;
				return RESULT_STAY;
			} else {
				return RESULT_ERROR;
			}
		case MyCommand.CMDNUM_GOSUB: //GOSUB
			mc2=getJumpto(mc,1);
			if (mc2!=null) {
				pgcStack.push(nowCommand);
				nowCommand=mc2; //mcLabel[mc.iData[2]];
				return RESULT_STAY;
			} else {
				return RESULT_ERROR;
			}
		case MyCommand.CMDNUM_RETURN: //RETURN
			mc2=pgcStack.pop();
			if (mc2!=null) {
				nowCommand=mc2;
			} else {
				iErr=ERRNUM_NORETURN; return RESULT_ERROR;
			}
			break;
		case MyCommand.CMDNUM_PRINT: //PRINT
			n=mc.iData[1];
			mytext.print(MyCommand.VAR_NAME[n]+" >> "+Integer.toString(iVar[n]));
			if (flushflag)
				canvas1.mypaint();
			break;
		case MyCommand.CMDNUM_MESSAGE: //MESSAGE
			n=mc.iData[1];
			if (mycommands.mes[n]!=null) {
				mytext.print(mycommands.mes[n]);
			} else {
				mytext.print("");
			}
			if (flushflag)
				canvas1.mypaint();
			break;
		case MyCommand.CMDNUM_INPUT: //INPUT
			n=mc.iData[1];
			inputflag=true;
			canvas1.inputstart("INPUT "+MyCommand.VAR_NAME[n]);
			while (inputflag);
			iVar[n]=inputdata;
			mytext.print(MyCommand.VAR_NAME[n]+" << "+Integer.toString(inputdata));
			if (flushflag)
				canvas1.mypaint();
			break;
		case MyCommand.CMDNUM_PUSH: //PUSH
			valueStack.push(selValue(mc,1));
			break;
		case MyCommand.CMDNUM_POP: //POP
			try {
				v=valueStack.pop();
				iVar[mc.iData[1]]=v;
			} catch (MyException me) {
				System.out.println("doCommand<POP>::"+me.toString());
				iErr=ERRNUM_NOSTACK;
				return RESULT_ERROR;
			}
			break;
		case MyCommand.CMDNUM_WAIT: //WAIT
			int df=selValue(mc,1);
			if (df<0) {
				iErr=ERRNUM_WRONGTIME; return RESULT_ERROR;
			} else {
				long tl=System.currentTimeMillis()+(long)df;
				while (System.currentTimeMillis()<tl) {
					if (cmd==CMDNUM_STOP) {
						iErr=ERRNUM_REQSTOP;
						return RESULT_REQSTOP;
					}
					if (finishFlag) {
						return RESULT_REQFINISH;
					}
				}
			}
			break;
		case MyCommand.CMDNUM_ABS:   //ABS
			iVar[mc.iData[1]]=Math.abs(iVar[mc.iData[2]]);
			break;
		case MyCommand.CMDNUM_SGN:   //SGN
			v=iVar[mc.iData[2]];
			if (v<0)      iVar[mc.iData[1]]=-1;
			else if (v>0) iVar[mc.iData[1]]=1;
			else          iVar[mc.iData[1]]=0;
			break;
		case MyCommand.CMDNUM_RAND:  //RAND
			v=selValue(mc,2);
			if (v<2) {
				iErr=ERRNUM_WRONGVALUE; return RESULT_ERROR;
			}
			iVar[mc.iData[1]]=rand(v);
			break;
		case MyCommand.CMDNUM_MAX:   //MAX
			iVar[mc.iData[1]]=Math.max(iVar[mc.iData[2]], selValue(mc,3));
			break;
		case MyCommand.CMDNUM_MIN:   //MIN
			iVar[mc.iData[1]]=Math.min(iVar[mc.iData[2]], selValue(mc,3));
			break;
		case MyCommand.CMDNUM_CLS:   //CLS
			if (mc.iData[1]<2)
				mytext.cls();
			if (mc.iData[1]!=1) {
				gg.setColor(0,0,0);
				gg.fillRect(0,0,w,h);
			}
			if (flushflag)
				canvas1.mypaint();
			break;
		case MyCommand.CMDNUM_FLUSH: //FLUSH
			if (mc.iData[1]==0) {
				flushflag=true;
			} else {
				if (flushflag)
					flushflag=false;
				else
					canvas1.mypaint();
			}
			break;
		case MyCommand.CMDNUM_COLOR: //COLOR
			switch (mc.iData[2]) {
			case MyCommand.CLRNUM_BLACK:
				r=0; g=0; b=0;
				break;
			case MyCommand.CLRNUM_WHITE:
				r=255; g=255; b=255;
				break;
			case MyCommand.CLRNUM_RED:
				r=255; g=0; b=0;
				break;
			case MyCommand.CLRNUM_GREEN:
				r=0; g=255; b=0;
				break;
			case MyCommand.CLRNUM_BLUE:
				r=0; g=0; b=255;
				break;
			case MyCommand.CLRNUM_MAGENTA:
				r=255; g=0; b=255;
				break;
			case MyCommand.CLRNUM_YELLOW:
				r=255; g=255; b=0;
				break;
			case MyCommand.CLRNUM_CYAN:
				r=0; g=255; b=255;
				break;
			default:
				r=128; g=128; b=128;
				break;
			}
			if (mc.iData[1]<2)
				mytext.setColor(r,g,b);
			if (mc.iData[1]!=1) {
				iR=r; iG=g; iB=b;
			}
			break;
		case MyCommand.CMDNUM_RESET: //RESET
			for (i=0;i<iVar.length;i++)
				iVar[i]=0;
			for (i=0;i<ARRAY_SIZE;i++)
				myArray[i]=0;
			mytext.setColor(255,255,255);
			iR=255; iG=255; iB=255; pX=0; pY=0; arcStart=0; arcAngle=360;
			flushflag=true;
			mytext.cls();
			gg.setColor(0,0,0);
			gg.fillRect(0,0,w,h);
			canvas1.mypaint();
			break;
		case MyCommand.CMDNUM_POSX:   //POSX
			pX=selValue(mc,1);
			break;
		case MyCommand.CMDNUM_POSY:   //POSY
			pY=selValue(mc,1);
			break;
		case MyCommand.CMDNUM_POSXY:  //POSXY
			pX=selValue(mc,1);
			pY=selValue(mc,3);
			break;
		case MyCommand.CMDNUM_DOT:    //DOT
			gg.setColor(iR,iG,iB);
			gg.drawLine(pX,pY,pX,pY);
			if (flushflag)
				canvas1.mypaint();
			break;
		case MyCommand.CMDNUM_LINE:   //LINE
			gg.setColor(iR,iG,iB);
			gg.drawLine(pX,pY,selValue(mc,1),selValue(mc,3));
			if (flushflag)
				canvas1.mypaint();
			break;
		case MyCommand.CMDNUM_RECT:   //RECT
			gg.setColor(iR,iG,iB);
			gg.drawRect(pX,pY,selValue(mc,1),selValue(mc,3));
			if (flushflag)
				canvas1.mypaint();
			break;
		case MyCommand.CMDNUM_ARCST:  //ARCST
			arcStart=selValue(mc,1);
			break;
		case MyCommand.CMDNUM_ARCAN:  //ARCAN
			arcAngle=selValue(mc,1);
			break;
		case MyCommand.CMDNUM_ARC:    //ARC
			gg.setColor(iR,iG,iB);
			gg.drawArc(pX,pY,selValue(mc,1),selValue(mc,3),arcStart,arcAngle);
			if (flushflag)
				canvas1.mypaint();
			break;
		case MyCommand.CMDNUM_RECTF:  //RECTF
			gg.setColor(iR,iG,iB);
			gg.fillRect(pX,pY,selValue(mc,1),selValue(mc,3));
			if (flushflag)
				canvas1.mypaint();
			break;
		case MyCommand.CMDNUM_ARCF:   //ARCF
			gg.setColor(iR,iG,iB);
			gg.fillArc(pX,pY,selValue(mc,1),selValue(mc,3),arcStart,arcAngle);
			if (flushflag)
				canvas1.mypaint();
			break;
		case MyCommand.CMDNUM_KEYSTAT: //KEYSTAT
			n=0; v=canvas1.getKeyStates();
			switch (mc.iData[1]) {
			case 13: if ((v & canvas1.UP_PRESSED)!=0)    n=1; break;
			case 14: if ((v & canvas1.DOWN_PRESSED)!=0)  n=2; break;
			case 15: if ((v & canvas1.LEFT_PRESSED)!=0)  n=3; break;
			case 16: if ((v & canvas1.RIGHT_PRESSED)!=0) n=4; break;
			case 17: if ((v & canvas1.FIRE_PRESSED)!=0)  n=5; break;
			default:
				if (mc.iData[1]<20) { 
					if (mc.iData[1]==keyNum) 
						n=6; 
				} else {
					if ((mc.iData[1]-20)!=keyNum)
						n=7;
				}
				break;
			}
			if (n>0) {
				mc2=getJumpto(mc,2);
				if (mc2!=null) {
					switch (mc.iData[4]) {
					case 3: case 4: case 5:
						pgcStack.push(nowCommand); break;
					}
					nowCommand=mc2;
					if (mc.iData[2]!=6)
						return RESULT_STAY;
				} else {
					
					return RESULT_ERROR;
				}
			}
			break;
		case MyCommand.CMDNUM_INKEY:   //INKEY
			iVar[mc.iData[1]]=keyNum;
			break;
		case MyCommand.CMDNUM_INC:   //INC
			vl=(long)iVar[mc.iData[1]]+1L;
			if ((vl<(long)Integer.MIN_VALUE)||(vl>(long)Integer.MAX_VALUE)) {
				iErr=ERRNUM_OVERFLOW; return RESULT_ERROR;
			}
			iVar[mc.iData[1]]=(int)vl;
			break;
		case MyCommand.CMDNUM_DEC:   //DEC
			vl=(long)iVar[mc.iData[1]]-1L;
			if ((vl<(long)Integer.MIN_VALUE)||(vl>(long)Integer.MAX_VALUE)) {
				iErr=ERRNUM_OVERFLOW; return RESULT_ERROR;
			}
			iVar[mc.iData[1]]=(int)vl;
			break;
		case MyCommand.CMDNUM_ADDME: //ADDME
			vl=(long)iVar[mc.iData[1]]+(long)selValue(mc,2);
			if ((vl<(long)Integer.MIN_VALUE)||(vl>(long)Integer.MAX_VALUE)) {
				iErr=ERRNUM_OVERFLOW; return RESULT_ERROR;
			}
			iVar[mc.iData[1]]=(int)vl;
			break;
		case MyCommand.CMDNUM_SUBME: //SUBME
			vl=(long)iVar[mc.iData[1]]-(long)selValue(mc,2);
			if ((vl<(long)Integer.MIN_VALUE)||(vl>(long)Integer.MAX_VALUE)) {
				iErr=ERRNUM_OVERFLOW; return RESULT_ERROR;
			}
			iVar[mc.iData[1]]=(int)vl;
			break;
		case MyCommand.CMDNUM_MULME: //MULME
			vl=(long)iVar[mc.iData[1]]*(long)selValue(mc,2);
			if ((vl<(long)Integer.MIN_VALUE)||(vl>(long)Integer.MAX_VALUE)) {
				iErr=ERRNUM_OVERFLOW; return RESULT_ERROR;
			}
			iVar[mc.iData[1]]=(int)vl;
			break;
		case MyCommand.CMDNUM_DIVME: //DIVME
			v=selValue(mc,2);
			if (v==0) {
				iErr=ERRNUM_DIVIDEZERO; return RESULT_ERROR;
			}
			vl=(long)iVar[mc.iData[1]]/(long)v;
			if ((vl<(long)Integer.MIN_VALUE)||(vl>(long)Integer.MAX_VALUE)) {
				iErr=ERRNUM_OVERFLOW; return RESULT_ERROR;
			} 
			iVar[mc.iData[1]]=(int)vl;
			break;
		case MyCommand.CMDNUM_REMME: //REMME
			v=selValue(mc,2);
			if (v==0) {
				iErr=ERRNUM_DIVIDEZERO; return RESULT_ERROR;
			}
			vl=(long)iVar[mc.iData[1]]%(long)v;
			if ((vl<(long)Integer.MIN_VALUE)||(vl>(long)Integer.MAX_VALUE)) {
				iErr=ERRNUM_OVERFLOW; return RESULT_ERROR;
			} 
			iVar[mc.iData[1]]=(int)vl;
			break;
		case MyCommand.CMDNUM_SQRT:  //SQRT
			v=selValue(mc,2);
			if (v<0) {
				iErr=ERRNUM_WRONGVALUE; return RESULT_ERROR;
			}
			d=Math.sqrt((double)v);
			vl=(long)d;
			if ((vl<(long)Integer.MIN_VALUE)||(vl>(long)Integer.MAX_VALUE)) {
				iErr=ERRNUM_OVERFLOW; return RESULT_ERROR;
			}
			iVar[mc.iData[1]]=(int)vl;
			break;
		case MyCommand.CMDNUM_SQRTME://SQRTME
			v=iVar[mc.iData[1]];
			if (v<0) {
				iErr=ERRNUM_WRONGVALUE; return RESULT_ERROR;
			}
			d=Math.sqrt((double)v);
			vl=(long)d;
			if ((vl<(long)Integer.MIN_VALUE)||(vl>(long)Integer.MAX_VALUE)) {
				iErr=ERRNUM_OVERFLOW; return RESULT_ERROR;
			}
			iVar[mc.iData[1]]=(int)vl;
			break;
		case MyCommand.CMDNUM_RVS:   //RVS
			iVar[mc.iData[1]]=-selValue(mc,2);
			break;
		case MyCommand.CMDNUM_RVSME: //RVSME
			v=iVar[mc.iData[1]];
			iVar[mc.iData[1]]=-v;
			break;
		case MyCommand.CMDNUM_ADDSF: //ADDSF
			vl=(long)iVar[mc.iData[1]]; vl+=vl;
			if ((vl<(long)Integer.MIN_VALUE)||(vl>(long)Integer.MAX_VALUE)) {
				iErr=ERRNUM_OVERFLOW; return RESULT_ERROR;
			}
			iVar[mc.iData[1]]=(int)vl;
			break;
		case MyCommand.CMDNUM_MULSF: //MULSF
			vl=(long)iVar[mc.iData[1]]; vl*=vl;
			if ((vl<(long)Integer.MIN_VALUE)||(vl>(long)Integer.MAX_VALUE)) {
				iErr=ERRNUM_OVERFLOW; return RESULT_ERROR;
			}
			iVar[mc.iData[1]]=(int)vl;
			break;
		case MyCommand.CMDNUM_RAISEME: //RAISEME
			v=selValue(mc,2);
			if (v<0) {
				iErr=ERRNUM_WRONGPOWER; return RESULT_ERROR;
			}
			vl=1L; vl2=(long)iVar[mc.iData[1]];
			for (i=0;i<v;i++) {
				vl*=vl2;
				if ((vl<(long)Integer.MIN_VALUE)||(vl>(long)Integer.MAX_VALUE)) {
					iErr=ERRNUM_OVERFLOW; return RESULT_ERROR;
				}
			}
			iVar[mc.iData[1]]=(int)vl;
			break;
		case MyCommand.CMDNUM_SIN:     //SIN
			d=Math.sin(Math.toRadians((double)selValue(mc,2)));
			d*=10000.0;
			iVar[mc.iData[1]]=(int)d;
			break;
		case MyCommand.CMDNUM_COS:     //COS
			d=Math.cos(Math.toRadians((double)selValue(mc,2)));
			d*=10000.0;
			iVar[mc.iData[1]]=(int)d;
			break;
		case MyCommand.CMDNUM_SWAP:     //SWAP
			v=iVar[mc.iData[1]];
			iVar[mc.iData[1]]=iVar[mc.iData[2]];
			iVar[mc.iData[2]]=v;
			break;
		case MyCommand.CMDNUM_LETARY:     //LETARY
			v=selValue(mc,1);
			if ((v<0)||(v>=ARRAY_SIZE)) {
				iErr=ERRNUM_WRONGINDEX; return RESULT_ERROR;
			}
			myArray[v]=selValue(mc,3);
			break;
		case MyCommand.CMDNUM_GETARY:     //GETARY
			v=selValue(mc,2);
			if ((v<0)||(v>=ARRAY_SIZE)) {
				iErr=ERRNUM_WRONGINDEX; return RESULT_ERROR;
			}
			iVar[mc.iData[1]]=myArray[v];
			break;
		case MyCommand.CMDNUM_INMAX:
			v=iVar[mc.iData[1]];
			iVar[mc.iData[1]]=Math.max(v, selValue(mc,2));
			break;
		case MyCommand.CMDNUM_INMIN:
			v=iVar[mc.iData[1]];
			iVar[mc.iData[1]]=Math.min(v, selValue(mc,2));
			break;
		case MyCommand.CMDNUM_GCD:
			v=gcd(iVar[mc.iData[2]],selValue(mc,3));
			if (v>0) {
				iVar[mc.iData[1]]=v;
			} else {
				iErr=ERRNUM_WRONGVALUE; return RESULT_ERROR;
			}
			break;
		case MyCommand.CMDNUM_LCM:
			v=lcm(iVar[mc.iData[2]],selValue(mc,3));
			if (v>0) {
				iVar[mc.iData[1]]=v;
			} else {
				iErr=ERRNUM_WRONGVALUE; return RESULT_ERROR;
			}
		}
		return RESULT_NEXT;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//乱数の計算
	private int rand(int num) {
		return (rndm.nextInt()>>>1)%num;
	} 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private int gcd(int a, int b) {
		int c=Math.max(a,b);
		int d=Math.min(a,b);
		if (d<1) return 0;
		int e=c%d;
		if (e==0) return d;
		return gcd(e,d);
	}
	
	private int lcm(int a, int b) {
		int c=gcd(a,b);
		if (c==0) return 0;
		return (a*b)/c;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //MyProgramの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
