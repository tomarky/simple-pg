// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MyText.java  クラス
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// インポート
//import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
//import java.io.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class MyText {

	int        iStart=0;
	int        iCount=0;
	String[]   sText=new String[20];
	int        iR=255, iG=255, iB=255;

	//コンストラクタ
	MyText() {
		int i;
		for (i=0;i<sText.length;i++)
			sText[i]="";
	}

	public void cls() {
		iStart=0;
		iCount=0;
	}

	public void setColor(int r, int g, int b) {
		iR=r;
		iG=g;
		iB=b;
	}

	public void print(String s) {
		if (iCount<sText.length) {
			sText[iCount]=new String(s);
			iCount++;
		} else {
			sText[iStart]=new String(s);
			iStart=(iStart+1)%sText.length;
		}
	}

	public void paint(Graphics g) {
		int i,j;
		g.setColor(iR,iG,iB);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		for (i=0;i<iCount;i++) {
			j=(iStart+i)%sText.length;
			g.drawString(sText[j],0,i*13,Graphics.LEFT|Graphics.TOP);
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //MyTextの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
