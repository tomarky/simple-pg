// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// Canvas1.java メイン処理部
//
// (C)Tomarky   2009.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//インポート
import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;
import java.util.*;
//import javax.microedition.rms.*;   //データセーブ用
import java.io.*;                  //バイト変換用
//import javax.microedition.media.*; //音楽再生

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//メイン処理(キャンバス)
class Canvas1 extends GameCanvas implements Runnable,CommandListener,ItemCommandListener {
	private final String   RS_PROGRAM="tm.simplepg.program";
	
	private final String   MES_PLEASEKEYFIRE="Please Press FIRE";
	
	public static boolean  detailFlag=false;
	
	private Graphics       g=null, gg=null;
	private Image          image=null;
	private int            keyEvent=-999;         //キーイベント
	private int            keyState;              //キー状態
	private boolean        bl=true;
	private boolean        cf=false;
	private Command[]      cmds=new Command[23];//コマンド
	private int            cmd=-1;
//	private Random         rndm=new Random();      //乱数

	private int            w=240;
	private int            h=268;
	private MIDlet         midlet;

	private MyText         mytext=new MyText();
	
	private Form1          form1=null;
	private int            inputdata=0;

	private Form2          form2=null;
	
	private Form3          formExp=null;
	
	private Form           form3=null, form4=null, form5=null,
	                       form6=null, form7=null, form8=null,
	                       form9=null, form10=null, form11=null;
	
	private final int      ST_IDLE   = 0, ST_LISTAL = 1, ST_EXIT   = 2, ST_LISTAP = 3,
	                       ST_ADDNEW = 4, ST_REVISE = 5, ST_INSERT = 6, ST_DELETE = 7,
	                       ST_RUN    = 8, ST_FILE   = 9, ST_EDIT   =10, ST_TEST   =11,
	                       ST_TEST2  =12, ST_SAVE   =13, ST_LOAD   =14, ST_NEW    =15,
	                       ST_SETMSG =16, ST_OTHER  =17, ST_VIEW   =18, ST_HELP   =19;
	private int            iState=ST_IDLE;
 
	private int            iListCount=0,iListOffset=0;

	private MyCommands     mycommands=new MyCommands();

	private MyProgram      mypg=null;
	
	private MyCommand      testCommand=null;
	
	private MySaveList     mysavelist=new MySaveList();
	
	private int            iSelect=0;
	private String         sSelect=null;

	private int            vPoint=0;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コンストラクタ
	Canvas1(MIDlet m) {
		//キーイベントの抑制
		super(false);
		
		//MIDlet操作用
		midlet=m;
		
		form2=new Form2(this);
		
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void init() {
		int i;
		
		//グラフィックスの取得
		g=getGraphics();

		//コマンドリスナーの指定
		setCommandListener(this);

		//コマンドの生成
		cmds[0]= new Command("Yes"   ,Command.OK    , 0);
		cmds[1]= new Command("No"    ,Command.CANCEL, 0);
		cmds[14]=new Command("Stop"  ,Command.STOP  , 0);
		
		cmds[18]=new Command("Back"  ,Command.SCREEN, 0);

		cmds[9]= new Command("Run"   ,Command.SCREEN, 0);
		cmds[16]=new Command("Edit"  ,Command.SCREEN, 1);
		cmds[17]=new Command("File"  ,Command.SCREEN, 2);
		cmds[19]=new Command("Other" ,Command.SCREEN, 3);
		cmds[2]= new Command("Exit"  ,Command.SCREEN, 4);

		cmds[5]= new Command("AddNew",Command.SCREEN, 1);
		cmds[6]= new Command("Revise",Command.SCREEN, 2);
		cmds[7]= new Command("Insert",Command.SCREEN, 3);
		cmds[8]= new Command("Delete",Command.SCREEN, 4);
		cmds[10]=new Command("Test"  ,Command.SCREEN, 5);
		cmds[3]= new Command("ListAL",Command.SCREEN, 6);
		cmds[4]= new Command("ListAp",Command.SCREEN, 7);
		cmds[15]=new Command("SetMsg",Command.SCREEN, 8);
		
		cmds[11]=new Command("Save"  ,Command.SCREEN, 1);
		cmds[12]=new Command("Load"  ,Command.SCREEN, 2);
		cmds[13]=new Command("New"   ,Command.SCREEN, 3);

		cmds[20]=new Command("Detail",Command.SCREEN, 1);
		cmds[21]=new Command("View"  ,Command.SCREEN, 2);
		cmds[22]=new Command("Help"  ,Command.SCREEN, 3);

		//コマンドの追加
		setCommand1(true);

		image=Image.createImage(w,h);
		gg=image.getGraphics();
		gg.setColor(0,0,0);
		gg.fillRect(0,0,w,h);

		//ロード
		mypg=new MyProgram(this,mytext,mycommands,gg);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void setCommand1(boolean b) { //Main Menu
		if (b) {
			addCommand(cmds[9]);  //run
			addCommand(cmds[16]); //edit
			addCommand(cmds[17]); //file
			addCommand(cmds[19]); //Other
			addCommand(cmds[2]);  //exit
		}
		else {
			removeCommand(cmds[9]);  //run
			removeCommand(cmds[16]); //edit
			removeCommand(cmds[17]); //file
			removeCommand(cmds[19]); //Other
			removeCommand(cmds[2]);  //exit
		}
	}
	
	private void setCommand2(boolean b) { //Yes No
		int i;
		if (b)
			for (i=0;i<2;i++)
				addCommand(cmds[i]);
		else
			for (i=0;i<2;i++)
				removeCommand(cmds[i]);
	}
	
	private void setCommand3(boolean b) { //OTHER
		if (b) {
			addCommand(cmds[18]); //back
			addCommand(cmds[20]); //detail
			addCommand(cmds[21]); //view
			addCommand(cmds[22]); //help
		} else {
			removeCommand(cmds[18]); //back
			removeCommand(cmds[20]); //detail
			removeCommand(cmds[21]); //view
			removeCommand(cmds[22]); //help
		}
	}

	private void setCommand4(boolean b) { //EDIT
		if (b) {
			addCommand(cmds[18]); //back
			addCommand(cmds[5]);  //addnew
			addCommand(cmds[6]);  //revise
			addCommand(cmds[7]);  //insert
			addCommand(cmds[8]);  //delete
			addCommand(cmds[10]); //test
			addCommand(cmds[3]);  //listal
			addCommand(cmds[4]);  //listap
			addCommand(cmds[15]); //setmsg
		}
		else {
			removeCommand(cmds[18]); //back
			removeCommand(cmds[5]);  //addnew
			removeCommand(cmds[6]);  //revise
			removeCommand(cmds[7]);  //insert
			removeCommand(cmds[8]);  //delete
			removeCommand(cmds[10]); //test
			removeCommand(cmds[3]);  //listal
			removeCommand(cmds[4]);  //listap
			removeCommand(cmds[15]); //setmsg
		}
	}

	private void setCommand5(boolean b) { //FILE
		if (b) {
			addCommand(cmds[18]); //back
			addCommand(cmds[11]); //save
			addCommand(cmds[12]); //load
			addCommand(cmds[13]); //new
		}
		else {
			removeCommand(cmds[18]); //back
			removeCommand(cmds[11]); //save
			removeCommand(cmds[12]); //load
			removeCommand(cmds[13]); //new
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//実行
	public void run() {
		long sleepTime=0L;
		
		init();
		
		mytext.print("Running");
		
		while (bl) { //メインループ @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		
			if (isShown()) {
				
				switch (iState) {
				case ST_EXIT:
					ml_exit();
					break;
					
				case ST_IDLE:
					ml_idle();
					break;
				
				case ST_LISTAL:
					ml_listal();
					break;
				
				case ST_EDIT:
					ml_edit();
					break;
				
				case ST_FILE:
					ml_file();
					break;
				
				case ST_DELETE:
					ml_delete();
					break;
				
				case ST_TEST2:
					ml_test();
					break;
				
				case ST_OTHER:
					ml_other();
					break;
				}
				
				//描写
				mypaint();
				
	            
				//スリープ
				while (System.currentTimeMillis()<sleepTime+100);
				sleepTime=System.currentTimeMillis();
			
			}
		}

		//終了
		if (cf==false) midlet.notifyDestroyed();

	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public void mypaint() {
		//描画
		g.drawImage(image,0,0,Graphics.LEFT|Graphics.TOP);
		mytext.paint(g);
		
		switch (iState) {
		case ST_EXIT:
			drawComment("Finish ?");
			break;
		case ST_DELETE:
			drawComment("Delete ?");
			break;
		}
		
		flushGraphics();
	}


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//キープレスイベント
	public void keyPressed(int keyCode) {
		if (keyCode==0) return;
		
		mypg.keyPressed(keyCode);
		
		switch(getGameAction(keyCode)) {        
		case FIRE:			//選択キー
			keyEvent=FIRE;
			break;
		}	
	}


	public void keyReleased(int keyCode) {
		if (keyCode==0) return;
		
		mypg.keyReleased(keyCode);

	}
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コマンドイベント
	public void commandAction(Command c,Displayable disp) {
		MyCommand mc=null;
		int p,i;

		cmd=-999;
		
		//form1（インプット）
		if (disp==form1) {
			inputend();
			switch (iState) {
			case ST_LISTAP:
				iListOffset=mycommands.NumCount(inputdata);
				iState=ST_LISTAL;
				break;
			case ST_REVISE:
				mc=mycommands.DataNum(inputdata);
				if (mc!=null) {
					//form2=new Form2(this,mc);
					form2.resetRevise(mc);
					ChangeCurrent(form2);
				} else {
					mytext.print("Wrong Number");
					setCommand4(true);
					iState=ST_EDIT;
					//iState=ST_IDLE;
				}
				break;
			case ST_DELETE:
				mc=mycommands.DataNum(inputdata);
				mytext.print("Delete "+inputdata);
				if (mc!=null){
					setCommand2(true);
					mytext.print(">>> "+mc.strData(detailFlag));
					mytext.print(" ");
				} else {
					mytext.print("Wrong Number");
					setCommand4(true);
					iState=ST_EDIT;
					//iState=ST_IDLE;
				}
				break;
			case ST_RUN:
			case ST_TEST2:
				mypg.input(inputdata);
				break;
			}
			return;
		}
		
		//fomr2（コマンド）
		if (disp==form2) {
			if (c.getCommandType()==Command.OK) {
				mc=form2.data();
				switch (iState) {
				case ST_ADDNEW:
					mycommands.addnew(mc);
					break;
				case ST_REVISE:
					mycommands.revise(mc);
					break;
				case ST_INSERT:
					if (mc.iNum<1) return;
					if (mc.iNum>mycommands.NextNum()) return;
					mycommands.insert(mc);
					break;
				case ST_TEST:
					mytext.print(">>> "+mc.strData(detailFlag));
					testCommand=mc;
					ChangeCurrent(this);
					addCommand(cmds[14]);
					//form2=null;
					iState=ST_TEST2;
					return;
				}
				mytext.print(mc.strData(detailFlag));
			
			} 
			ChangeCurrent(this);
			//form2=null;
			setCommand4(true);
			iState=ST_EDIT;
			//iState=ST_IDLE;
			return;
		}
		
		//セーブ・ロード、リスト
		if (disp==form3) {
			p=c.getPriority();
			switch (p) {
			case 0: //Back
				ChangeCurrent(this);
				form3=null;
				iState=ST_FILE;
				break;
			case 1: //SaveAs
				setProgramName();
				break;
			}
			return;
		}
		
		//プログラム名設定
		if (disp==form4) {
			if (c.getCommandType()==Command.OK) {
				TextField tf1=(TextField)form4.get(0);
				String nm=tf1.getString();
				if (nm==null) nm="myprogram";
				if (nm.length()==0) nm="myprogram";
				int r=MyRecord.saveNewData(RS_PROGRAM,nm,mycommands);
				if (r==MyRecord.RESULT_SUCCESS)
					mytext.print("success to saveas");
				else
					mytext.print("error("+Integer.toString(r)+"): faild to saveas");
				form3=null;
				setSaveListForm();
				form4=null;
			} else {
				ChangeCurrent(form3);
				form4=null;
			}
			return;
		}
		
		//ロード確認
		if (disp==form5) {
			if (c.getCommandType()==Command.OK) {
				mycommands.AllClear();
				int r=MyRecord.loadData(RS_PROGRAM,iSelect,mycommands);
				if (r==MyRecord.RESULT_SUCCESS)
					mytext.print("success to load");
				else
					mytext.print("error("+Integer.toString(r)+"): faild to load");
				ChangeCurrent(this);
				form3=null;
				form5=null;
				iState=ST_FILE;
				cmd=18;
			} else {
				ChangeCurrent(form3);
				form5=null;
			}
			return;
		}
		
		//削除確認(load)
		if (disp==form6) {
			if (c.getCommandType()==Command.OK) {
				int r=MyRecord.deleteData(RS_PROGRAM,iSelect);
				if (r==MyRecord.RESULT_SUCCESS)
					mytext.print("success to delete");
				else
					mytext.print("error("+Integer.toString(r)+"): faild to delete");			
				form3=null;
				setLoadListForm();
				form6=null;
			} else {
				ChangeCurrent(form3);
				form6=null;
			}
			return;
		}

		//削除確認(save)
		if (disp==form7) {
			if (c.getCommandType()==Command.OK) {
				int r=MyRecord.deleteData(RS_PROGRAM,iSelect);
				if (r==MyRecord.RESULT_SUCCESS)
					mytext.print("success to delete");
				else
					mytext.print("error("+Integer.toString(r)+"): faild to delete");
				form3=null;
				setSaveListForm();
				form7=null;
			} else {
				ChangeCurrent(form3);
				form7=null;
			}
			return;
		}
		
		//上書き確認
		if (disp==form8) {
			if (c.getCommandType()==Command.OK) {
				int r=MyRecord.saveUpdateData(RS_PROGRAM,iSelect,sSelect,mycommands);
				if (r==MyRecord.RESULT_SUCCESS)
					mytext.print("success to save");
				else
					mytext.print("error("+Integer.toString(r)+"): faild to save");
				form3=null;
				setSaveListForm();
				form8=null;
			} else {
				ChangeCurrent(form3);
				form8=null;
			}
			return;
		}
		
		//新規作成
		if (disp==form9) {
			if (c.getCommandType()==Command.OK) {
				mycommands.AllClear();
				mytext.print("Destroyed");
				ChangeCurrent(this);
				form9=null;
				iState=ST_FILE;
				cmd=18;
			} else {
				ChangeCurrent(this);
				form9=null;
				iState=ST_FILE;
			}
			return;
		}
		
		//メッセージ作成
		if (disp==form10) {
			if (c.getCommandType()==Command.OK) {
				for (i=0;i<MyCommands.MES_SIZE;i++) {
					MyCommands.mes[i]=((TextField)form10.get(i)).getString();
				}
			}
			ChangeCurrent(this);
			form10=null;
			iState=ST_EDIT;
		}
		
		//ビューフォーム
		if (disp==form11) {
			String tx=null,ti;
			TextField tf1;
			int n,m;
			p=c.getPriority();
			switch (p) {
			case 0:
				ChangeCurrent(this);
				form11=null;
				iState=ST_OTHER;
				return;
			case 1: //next
				tx=getViewText(vPoint+15);
				break;
			case 2: //back
				tx=getViewText(vPoint-15);
				break;
			}
			if (tx!=null) {
				m=mycommands.count();
				n=Math.min(vPoint+14,m);
				ti="Source "+Integer.toString(vPoint)+" - "
				       +Integer.toString(n)+" / "+Integer.toString(m);
				tf1=(TextField)form11.get(0);
				tf1.setLabel(ti);
				tf1.setString(tx);
			}
			return;
		}
		
		if (disp==formExp) {
			ChangeCurrent(this);
			formExp=null;
			iState=ST_OTHER;
			return;
		}
		
		for (i=0;i<cmds.length;i++)
			if (c==cmds[i]) {
				cmd=i;
				mypg.commandAction(cmd);
				return;
			}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//アイテム コマンドイベント
	public void commandAction(Command c, Item item) {
		int p=c.getPriority();
		
		switch (p) {
		case 2: //Save
			checkSave(item);
			break;
		case 3: //Delete(save)
			form7=checkDelete(item);
			break;
		case 4: //Load
			checkLoad(item);
			break;
		case 5: //Delete(load)
			form6=checkDelete(item);
			break;
		}
	}
 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private void ChangeCurrent(Displayable d) {
		(Display.getDisplay(midlet)).setCurrent(d);
	}

	public void inputstart(String title) {
		form1=new Form1(this,title);
		ChangeCurrent(form1);
	}
	
	private void inputend() {
		inputdata=form1.data();
		ChangeCurrent(this);
		form1=null;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コメント描写
	public void drawComment(String s) {
		g.setColor(255,255,255);
		g.fillRect(0,h-20,w,20);
		g.setColor(0,128,255);
		g.drawRect(0,h-20,w,20);
		g.setColor(0,0,0);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		g.drawString(s,w/2,h-18,Graphics.HCENTER|Graphics.TOP);
	} 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

/*	//中抜き文字の描写
	private void drawNukiMoji(String s, int x, int y, int rIn, int gIn, int bIn, int anchor) {
		int i,j;
		
		for (i=-1;i<2;i++)
			for (j=-1;j<2;j++)
				g.drawString(s,x+j,y+i,anchor);
		g.setColor(rIn,gIn,bIn);
		g.drawString(s,x,y,anchor);
	}
	private void drawNukiMoji(String s, int x, int y, int rIn,   int gIn,  int bIn,
	                                                  int rOut,  int gOut, int bOut, int anchor) {
		g.setColor(rOut,gOut,bOut);
		drawNukiMoji(s,x,y,rIn,gIn,bIn,anchor);
	} */

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

/*	//乱数の計算
	private int rand(int num) {
		return (rndm.nextInt()>>>1)%num;
	} */

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//終了命令
	public void callFinish() {
		mypg.finish();
		bl=false;
		cf=true;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private String getViewText(int newPoint) {
		ByteArrayOutputStream out=new ByteArrayOutputStream(4);
		PrintStream ps=new PrintStream(out);
		int n=mycommands.count();
		if (n==0) return null;
		if (newPoint<1) return null;
		if (newPoint>n) return null;
		int c=Math.min(n,newPoint+14);
		MyCommand mc=mycommands.DataNum(newPoint);
		int i;
		for (i=newPoint;i<=c;i++) {
			if (i<c)
				ps.println(mc.strData(detailFlag));
			else
				ps.print(mc.strData(detailFlag));
			mc=mc.next;
			if (mc==null)
				break;
		}
		String s=new String(out.toString());
		vPoint=newPoint;
		return s;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//セーブリスト
	private void setSaveListForm() {
		
		mysavelist.reset();
		int r=MyRecord.loadDataList(RS_PROGRAM,mysavelist);
		form3=new Form("PROGRAM LIST (SAVE)");
		form3.setCommandListener(this);
		
		StringItem si1=new StringItem(null,"Free  "+Integer.toString(mysavelist.size)+" B\n");
		form3.append(si1);

		si1=new StringItem(null,"Used  "+Integer.toString(mysavelist.usedsize)+" B\n");
		form3.append(si1);
		
		int cc=mycommands.toBytes().length;
		si1=new StringItem(null,"Now Edit  "+Integer.toString(cc+15)+"~"+Integer.toString(cc+60)+" B\n");
		form3.append(si1);
		
		form3.addCommand(new Command("Back"  ,Command.SCREEN,0));
		form3.addCommand(new Command("SaveAs",Command.SCREEN,1));
		
		Command cmd1=new Command("Save"  ,Command.SCREEN,2);
		Command cmd2=new Command("Delete",Command.SCREEN,3);
		int n=mysavelist.count();
		int i;
		TextField tf1;
		for (i=0;i<n;i++) {
			tf1=mysavelist.getTextField(i);
			tf1.addCommand(cmd1);
			tf1.addCommand(cmd2);
			tf1.setItemCommandListener(this);
			form3.append(tf1);
		}
		ChangeCurrent(form3);
	}

	//ロードリスト
	private void setLoadListForm() {
		
		mysavelist.reset();
		int r=MyRecord.loadDataList(RS_PROGRAM,mysavelist);
		form3=new Form("PROGRAM LIST (LOAD)");
		form3.setCommandListener(this);
		
		form3.addCommand(new Command("Back"  ,Command.SCREEN,0));
		
		Command cmd1=new Command("Load"  ,Command.SCREEN,4);
		Command cmd2=new Command("Delete",Command.SCREEN,5);
		int n=mysavelist.count();
		int i;
		TextField tf1;
		for (i=0;i<n;i++) {
			tf1=mysavelist.getTextField(i);
			tf1.addCommand(cmd1);
			tf1.addCommand(cmd2);
			tf1.setItemCommandListener(this);
			form3.append(tf1);
		}
		ChangeCurrent(form3);
	}
	
	//プログラム名設定
	private void setProgramName() {
		form4=new Form("SAVE AS");
		form4.setCommandListener(this);
		
		form4.append(new TextField("Program Title","myprogram",12,TextField.ANY));
		
		form4.addCommand(new Command("OK"    ,Command.OK    ,0));
		form4.addCommand(new Command("Cancel",Command.CANCEL,0));
		
		ChangeCurrent(form4);
	}
	
	private void checkLoad(Item item) {
		TextField tf1=(TextField)item;
		
		iSelect=mysavelist.RecordId(tf1);
		
		form5=new Form("LOAD");
		form5.setCommandListener(this);
		
		form5.append(new TextField(tf1.getLabel(),tf1.getString(),50,TextField.UNEDITABLE));
		form5.append(new StringItem(null,"\n\nLoad This?"));
		
		form5.addCommand(new Command("OK"    ,Command.OK    ,0));
		form5.addCommand(new Command("Cancel",Command.CANCEL,0));
		
		ChangeCurrent(form5);
	}

	private void checkSave(Item item) {
		TextField tf1=(TextField)item;
		
		iSelect=mysavelist.RecordId(tf1);
		sSelect=mysavelist.Name(tf1);
		
		form8=new Form("SAVE");
		form8.setCommandListener(this);
		
		form8.append(new TextField(tf1.getLabel(),tf1.getString(),50,TextField.UNEDITABLE));
		form8.append(new StringItem(null,"\n\nOverwrite This?"));
		
		form8.addCommand(new Command("OK"    ,Command.OK    ,0));
		form8.addCommand(new Command("Cancel",Command.CANCEL,0));
		
		ChangeCurrent(form8);
	}

	private Form checkDelete(Item item) {
		Form      formX=null;
		TextField tf1=(TextField)item;
		
		iSelect=mysavelist.RecordId(tf1);
		
		formX=new Form("DELETE");
		formX.setCommandListener(this);
		
		formX.append(new TextField(tf1.getLabel(),tf1.getString(),50,TextField.UNEDITABLE));
		formX.append(new StringItem(null,"\n\nDelete This?"));
		
		formX.addCommand(new Command("OK"    ,Command.OK    ,0));
		formX.addCommand(new Command("Cancel",Command.CANCEL,0));
		
		ChangeCurrent(formX);
		return formX;
	}
	
	private void checkNew() {
		form9=new Form("NEW PROGRAM");
		form9.setCommandListener(this);
		
		form9.append(new StringItem(null,"Destroy now editing program.\nAre you OK?"));
		
		form9.addCommand(new Command("OK"    ,Command.OK    ,0));
		form9.addCommand(new Command("Cancel",Command.CANCEL,0));
		
		ChangeCurrent(form9);

	}

	private void setMsgForm() {
		form10=new Form("SET MESSAGE");
		form10.setCommandListener(this);
		
		TextField tf1;
		int i;
		for (i=0;i<MyCommands.MES_SIZE;i++) {
			tf1=new TextField("Message "+Integer.toString(i+1),
			                   MyCommands.mes[i],MyCommands.MES_LENGTH,TextField.ANY);
			tf1.setLayout(Item.LAYOUT_NEWLINE_BEFORE);
			form10.append(tf1);
		}
		
		form10.addCommand(new Command("Set" ,Command.OK  ,0));
		form10.addCommand(new Command("Back",Command.BACK,0));
		
		ChangeCurrent(form10);
	}

	private void setViewForm() {
		form11=new Form("VIEW");
		form11.setCommandListener(this);
		
		int n=mycommands.count();
		int c=Math.min(15,n);
		form11.append(new TextField("Source 1 - "+Integer.toString(c)+" / "+Integer.toString(n),
		                             getViewText(1),2000,TextField.ANY));
		
		
		form11.addCommand(new Command("Close",Command.SCREEN,0));
		form11.addCommand(new Command("Next" ,Command.SCREEN,1));
		form11.addCommand(new Command("Back" ,Command.SCREEN,2));
		
		ChangeCurrent(form11);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void ml_exit() {
		switch (cmd) {
		case 0: //yes
			setCommand2(false);
			bl=false;
			break;
		case 1: //no
			setCommand2(false);
			setCommand1(true);
			iState=ST_IDLE;
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void ml_edit() {
		switch (cmd) {
		case 3:  //listAL
			setCommand4(false);
			addCommand(cmds[14]);
			iListCount=0;
			iListOffset=0;
			mytext.print(MES_PLEASEKEYFIRE);
			keyEvent=-999;
			iState=ST_LISTAL;
			break;
		case 4:  //listAp
			setCommand4(false);
			addCommand(cmds[14]);
			iListCount=0;
			iListOffset=0;
			mytext.print(MES_PLEASEKEYFIRE);
			keyEvent=-999;
			iState=ST_LISTAP;
			inputstart("START NUMBER");
			break;
		case 5:  //AddNew
			setCommand4(false);
			mytext.print("AddNew");
			//form2=new Form2(this,mycommands.NextNum(),"ADDNEW");
			form2.reset(mycommands.NextNum(),"ADDNEW");
			iState=ST_ADDNEW;
			ChangeCurrent(form2);
			break;
		case 6:  //Revise
			setCommand4(false);
			mytext.print("Revise");
			iState=ST_REVISE;
			inputstart("REVISE NUMBER");
			break;
		case 7:  //Insert
			setCommand4(false);
			mytext.print("Insert");
			//form2=new Form2(this);
			form2.resetInsert();
			iState=ST_INSERT;
			ChangeCurrent(form2);
			break;
		case 8:  //Delete
			setCommand4(false);
			iState=ST_DELETE;
			inputstart("DELETE NUMBER");
			break;
		case 10: //Test
			setCommand4(false);
			mytext.print("Test");
			//form2=new Form2(this,0,"TEST");
			form2.reset(0,"TEST");
			iState=ST_TEST;
			ChangeCurrent(form2);
			break;
		case 15:
			setMsgForm();
			iState=ST_SETMSG;
			break;
		case 18: //Back
			setCommand4(false);
			mytext.print("mode main");
			setCommand1(true);
			iState=ST_IDLE;
			break;
		}
		cmd=-999;
	}
	
	private void ml_file() {
		switch (cmd) {
		case 11: //Save
			setSaveListForm();
			iState=ST_SAVE;
			break;
		case 12: //load
			setLoadListForm();
			iState=ST_LOAD;
			break;
		case 13: //New
			checkNew();
			iState=ST_NEW;
			break;
		case 18: //Back
			setCommand5(false);
			mytext.print("mode main");
			setCommand1(true);
			iState=ST_IDLE;
			break;
		}
		cmd=-999;
	}
	
	private void ml_delete() {
		switch (cmd) {
		case 0: //yes
			setCommand2(false);
			mycommands.delNum(inputdata,true);
			mytext.print("success");
			setCommand4(true);
			iState=ST_EDIT;
			break;
		case 1: //no
			setCommand2(false);
			setCommand4(true);
			mytext.print("cancel");
			iState=ST_EDIT;
			break;
		}
		cmd=-999;	
	}

	private void ml_test() {
		if (testCommand!=null) {
			int r=mypg.doCommand(testCommand);
			mytext.print("Finish "+Integer.toString(r));
			testCommand=null;
		}
		removeCommand(cmds[14]);
		setCommand4(true);
		iState=ST_EDIT;
		//iState=ST_IDLE;
	}
	
	private void ml_other() {
		switch (cmd) {
		case 18: //back
			setCommand3(false);
			mytext.print("mode main");
			setCommand1(true);
			iState=ST_IDLE;
			break;
		case 20: //detail
			detailFlag=!detailFlag;
			if (detailFlag) {
				mytext.print("Detail On");
			} else {
				mytext.print("Detail Off");
			}
			break;
		case 21: //view
			setViewForm();
			iState=ST_VIEW;
			break;
		case 22: //help
			formExp=new Form3(this,midlet.getAppProperty("MIDlet-Version"));
			ChangeCurrent(formExp);
			iState=ST_HELP;
			break;
		}
		cmd=-999;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void ml_idle() {
		//コマンドイベントの処理
		switch (cmd) {
		case 2:  //exit
			setCommand1(false);
			setCommand2(true);
			iState=ST_EXIT;
			break;
		
		case 9:  //run
			mypg.resetRun(); 
			setCommand1(false);
			addCommand(cmds[14]);
			iState=ST_RUN;
			mytext.print("Run");
			mypaint();
			mypg.doRun();
			removeCommand(cmds[14]);
			setCommand1(true);
			iState=ST_IDLE;
			
			break;
		case 16:  //edit
			setCommand1(false);
			mytext.print("mode edit");
			setCommand4(true);
			iState=ST_EDIT;
			break;
		case 17:  //file
			setCommand1(false);
			mytext.print("mode file");
			setCommand5(true);
			iState=ST_FILE;
			break;
		case 19: //other
			setCommand1(false);
			mytext.print("mode other");
			setCommand3(true);
			iState=ST_OTHER;
			break;
		}
		cmd=-999;
		
		keyEvent=-999;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void ml_listal() {
		int lc=iListCount+iListOffset;
		
		if (cmd==14) { //stop
			mytext.print("Stop");
			removeCommand(cmds[14]);
			setCommand4(true);
			iState=ST_EDIT;
			cmd=-999;
			return;
		}
		cmd=-999;
		
		if (lc>=mycommands.count()) {
			if (keyEvent==FIRE) {
				mytext.print("End of List");
				removeCommand(cmds[14]);
				setCommand4(true);
				iState=ST_EDIT;
			}
			keyEvent=-999;
		} else {
			if ((iListCount%18)==0) {
				if (keyEvent==FIRE) {
					mytext.print(mycommands.Data(lc).strData(detailFlag));
					iListCount++;
					if (lc+1==mycommands.count()) {
						mytext.print(MES_PLEASEKEYFIRE);
						keyEvent=-999;
					}
				}
				keyEvent=-999;
			} else {
				mytext.print(mycommands.Data(lc).strData(detailFlag));
				iListCount++;
				if (((iListCount%18)==0)||(lc+1==mycommands.count())) {
					mytext.print(MES_PLEASEKEYFIRE);
					keyEvent=-999;
				}
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //Canvas1の終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
