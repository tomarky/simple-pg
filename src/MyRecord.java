// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MyRecord.java  レコードストア処理
//
// (C)Tomarky   2009.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//インポート
//import javax.microedition.lcdui.*;
//import javax.microedition.lcdui.game.*;
//import javax.microedition.midlet.*;
//import java.util.*;
import javax.microedition.rms.*;
import java.io.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class MyRecord {

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public static final int RESULT_SUCCESS     =  0,
	                        RESULT_NOLIST      = -1,
	                        RESULT_NOCOMMAND   = -2,
	                        RESULT_TOBYTEERROR = -3,
	                        RESULT_SAVEERROR   = -4,
	                        RESULT_NOSPACE     = -5,
	                        RESULT_LOADERROR   = -6,
	                        RESULT_READERROR   = -7,
	                        RESULT_DELERROR     = -8;

	//コンストラクタ
	public MyRecord() {
	}
	
	//新しいプログラムを記録
	public static int saveNewData(String recordname, String name, MyCommands mcs) {
		byte[] bmcs=mcs.toBytes();
		
		if (bmcs==null) return RESULT_NOCOMMAND;
		
		ByteArrayOutputStream out=new ByteArrayOutputStream(4);
		DataOutputStream      dos=new DataOutputStream(out);
		
		byte[] bData=null;
		
		try {
			byte[] b=name.getBytes();
			int    c=b.length;
			long   dt=System.currentTimeMillis();
			
			dos.writeLong(dt); //作成日付
			dos.writeInt(c);   //プログラム名のサイズ
			dos.write(b,0,c);  //プログラム名
			
			int    n=bmcs.length; //プログラムデータのバイトサイズ取得
			dos.writeInt(n);
			dos.write(bmcs,0,n);  //プログラムデータ
			
			bData=out.toByteArray();
			
			dos.close();

		} catch (Exception e) {
			System.out.println("MyRecord.saveNewData(toBytes)::"+e.toString());
			if (dos!=null) {
				try { dos.close();
				} catch (Exception e2) { System.out.println("+dis.close"+e2.toString()); }
			}
			dos=null; out=null; bData=null;
			return RESULT_TOBYTEERROR;
		}
		dos=null; out=null;
		
		RecordStore rs=null;
		
		try {
			rs=RecordStore.openRecordStore(recordname,true);
			rs.addRecord(bData,0,bData.length); //レコードストアへ記録
			rs.closeRecordStore();

		} catch (RecordStoreFullException rsfe) {
			System.out.println("MyRecord.saveNewData(Record[NOSPACE])::"+rsfe.toString());
			if (rs!=null) {
				try { rs.closeRecordStore();
				} catch (Exception e2) { System.out.println("+rs.close"+e2.toString()); }
			}
			rs=null; bData=null;
			return RESULT_NOSPACE;

		} catch (Exception e) {
			System.out.println("MyRecord.saveNewData(Record)::"+e.toString());
			if (rs!=null) {
				try { rs.closeRecordStore();
				} catch (Exception e2) { System.out.println("+rs.close"+e2.toString()); }
			}
			rs=null; bData=null;
			return RESULT_SAVEERROR;
		}
		rs=null; bData=null;
		
		return RESULT_SUCCESS;
	}
	
	//プログラムの更新
	public static int saveUpdateData(String recordname, int recordId, String name, MyCommands mcs) {
		byte[] bmcs=mcs.toBytes();
		
		if (bmcs==null) return RESULT_NOCOMMAND;
		
		ByteArrayOutputStream out=new ByteArrayOutputStream(4);
		DataOutputStream      dos=new DataOutputStream(out);
		
		byte[] bData=null;
		
		try {
			byte[] b=name.getBytes();
			int    c=b.length;
			long   dt=System.currentTimeMillis();
			
			dos.writeLong(dt); //作成日付
			dos.writeInt(c);   //プログラム名のサイズ
			dos.write(b,0,c);  //プログラム名
			
			int    n=bmcs.length; //プログラムデータのバイトサイズ取得
			dos.writeInt(n);
			dos.write(bmcs,0,n);  //プログラムデータ
			
			bData=out.toByteArray();
			
			dos.close();

		} catch (Exception e) {
			System.out.println("MyRecord.saveUpdateData(toBytes)::"+e.toString());
			if (dos!=null) {
				try { dos.close();
				} catch (Exception e2) { System.out.println("+dis.close"+e2.toString()); }
			}
			dos=null; out=null; bData=null;
			return RESULT_TOBYTEERROR;
		}
		dos=null; out=null;
		
		RecordStore rs=null;
		
		try {
			rs=RecordStore.openRecordStore(recordname,true);
			rs.setRecord(recordId,bData,0,bData.length); //レコードストアへ上書き保存
			rs.closeRecordStore();

		} catch (RecordStoreFullException rsfe) {
			System.out.println("MyRecord.saveNewData(Record[NOSPACE])::"+rsfe.toString());
			if (rs!=null) {
				try { rs.closeRecordStore();
				} catch (Exception e2) { System.out.println("+rs.close"+e2.toString()); }
			}
			rs=null; bData=null;
			return RESULT_NOSPACE;

		} catch (Exception e) {
			System.out.println("MyRecord.saveUpdateData(Record)::"+e.toString());
			if (rs!=null) {
				try { rs.closeRecordStore();
				} catch (Exception e2) { System.out.println("+rs.close"+e2.toString()); }
			}
			rs=null; bData=null;
			return RESULT_SAVEERROR;
		}
		rs=null; bData=null;
		
		return RESULT_SUCCESS;
	}
	
	//プログラムのリストをロード
	public static int loadDataList(String recordname, MySaveList msl) {
		msl.reset();
		
		RecordStore           rs=null;
		RecordEnumeration     re=null;
		ByteArrayInputStream  in=null;
		DataInputStream       dis=null;
		byte[] bData=null;
		
		try {
			rs=RecordStore.openRecordStore(recordname,true);
			re=rs.enumerateRecords(null,null,false);
			
			msl.size=rs.getSizeAvailable();
			msl.usedsize=rs.getSize();
			
			int n=rs.getNumRecords();
			int i,rid,c,sz;
			long dt;
			byte[] b;

			for (i=0;i<n;i++) {
				rid=re.nextRecordId();

				sz=rs.getRecordSize(rid); //レコードサイズの取得
				

				bData=rs.getRecord(rid); 

				in=new ByteArrayInputStream(bData);
				dis=new DataInputStream(in);
				
				dt=dis.readLong(); //最終更新日付の取得
				c=dis.readInt();   //プログラム名のサイズの取得
				b=new byte[c];     
				dis.read(b);       //プログラム名の取得

				msl.add(rid,new String(b),dt,sz);

				dis.close(); dis=null; in=null;
			}
			
			re.destroy();
			rs.closeRecordStore();
			
		} catch (Exception e) {
			System.out.println("MyRecord.loadDataList::"+e.toString());
			if (re!=null) {
				try { re.destroy(); 
				} catch (Exception e2) { System.out.println("+re.destroy::"+e2.toString()); }
			}
			if (rs!=null) {
				try { rs.closeRecordStore();
				} catch (Exception e2) { System.out.println("+rs.close::"+e2.toString()); }
			}
			if (dis!=null) {
				try { dis.close();
				} catch (Exception e2) { System.out.println("+dis.close::"+e2.toString()); }
			}
			re=null; rs=null; dis=null; in=null; bData=null;
			msl.reset();
			return RESULT_NOLIST;
		}
		
		return RESULT_SUCCESS;
	}

	//プログラムのロード
	public static int loadData(String recordname, int recordId, MyCommands mcs) {
		
		RecordStore rs=null;

		byte[] bData=null;

		try {
			rs=RecordStore.openRecordStore(recordname,false); 
			bData=rs.getRecord(recordId); 
			rs.closeRecordStore();

		} catch (Exception e) {
			System.out.println("MyRecord.loadData(Record)::"+e.toString());
			if (rs!=null) {
				try { rs.closeRecordStore();
				} catch (Exception e2) { System.out.println("+rs.close"+e2.toString()); }
			}
			return RESULT_LOADERROR;
		}
		rs=null;
		
		ByteArrayInputStream in=new ByteArrayInputStream(bData);
		DataInputStream      dis=new DataInputStream(in);
		
		mcs.AllClear();
		
		try {
			dis.readLong();      //日付
			int c=dis.readInt();
			byte[] b=new byte[c];
			dis.read(b);         //プログラム名
			dis.readInt();       //プログラムのデータサイズ
			
			int ver=dis.readInt(); //バージョン
			int i;
			
			int m=dis.readInt();
			
			m=Math.min(m,MyCommands.MES_SIZE);
			

			for (i=0;i<m;i++) { //文字列取得
				c=dis.readInt();
				if (c<0) {
					mcs.mes[i]=null;
				} else if (c==0) {
					mcs.mes[i]="";
				} else {
					b=new byte[c];
					dis.read(b);
					mcs.mes[i]=new String(b);
				}
			}
			
			int n=dis.readInt();   //コマンド数
			for (i=0;i<n;i++) {
				mcs.addnew(new MyCommand(dis));
			}
			
			dis.close();
			
		} catch (Exception e) {
			System.out.println("MyRecord.loadData(Read)::"+e.toString());
			try { dis.close();
			} catch (Exception e2) { System.out.println("+dis.close"+e2.toString()); }
			dis=null; in=null; bData=null;
			mcs.AllClear();
			return RESULT_READERROR;
		}
		dis=null; in=null; bData=null;
		
		return RESULT_SUCCESS;
	}

	public static int deleteData(String recordname, int recordId) {
		
		RecordStore rs=null;
		
		try {
			rs=RecordStore.openRecordStore(recordname,false);
			rs.deleteRecord(recordId);
			rs.closeRecordStore();

		} catch (Exception e) {
			System.out.println("MyRecord.deleteData::"+e.toString());
			if (rs!=null) {
				try { rs.closeRecordStore(); 
				} catch (Exception e2) { System.out.println("+rs.close"+e2.toString()); }
			}
			return RESULT_DELERROR;
		}
		return RESULT_SUCCESS;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //


} //MyRecordの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
