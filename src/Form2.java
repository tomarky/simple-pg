// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// Form2.java  クラス
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// インポート
//import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
//import java.io.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class Form2 extends Form implements ItemStateListener {

	private TextField    tfNum, tfCnst1, tfCnst2, tfAbsNum, tfRelNum, tfDetail;
	private ChoiceGroup  cgCmdType, cgCmd1, cgCmd2, cgCmd3, cgCmd4;
	private ChoiceGroup  cgVar0, cgVar1, cgVar2, cgMsg, cgKey;
	private ChoiceGroup  cgType1, cgType2, cgLabel1, cgJump, cgCmpJump,
	                     cgScreen, cgColor, cgOnOff;

	private int          mycount=0;

	//コンストラクタ
	Form2(CommandListener c) {
		super("");

		int i;
		
		//設定用文字列群
		String[] s1={"1","2","3","4"};
		String[] s2={"VARIABLE","CONSTANT"};
		String[] s3={"ON","OFF"};
		String[] s4={"LABEL","ABSOLUTE NUMBER","RELATIVE NUMBER"};
		String[] s5={"GT LABEL","GT ABSOLUTE NUMBER","GT RELATIVE NUMBER",
		             "GS LABEL","GS ABSOLUTE NUMBER","GS RELATIVE NUMBER", "RETURN"};

		//行番号
		tfNum=new TextField("NUMBER","0",11,TextField.UNEDITABLE);
		
		//コマンドタイプとコマンド
		cgCmdType=new ChoiceGroup("COMMAND TYPE",ChoiceGroup.POPUP,s1,null);
		cgCmd1=new ChoiceGroup("COMMAND",ChoiceGroup.POPUP,MyCommand.CMD_NAME1,null);
		cgCmd2=new ChoiceGroup("COMMAND",ChoiceGroup.POPUP,MyCommand.CMD_NAME2,null);
		cgCmd3=new ChoiceGroup("COMMAND",ChoiceGroup.POPUP,MyCommand.CMD_NAME3,null);
		cgCmd4=new ChoiceGroup("COMMAND",ChoiceGroup.POPUP,MyCommand.CMD_NAME4,null);
		
		//変数０
		cgVar0=new ChoiceGroup(s2[0]+"-0",ChoiceGroup.POPUP,MyCommand.VAR_NAME,null);

		//変数･定数１
		cgType1=new ChoiceGroup("TYPE-1"  ,ChoiceGroup.POPUP,s2,null);
		cgVar1=new ChoiceGroup(s2[0]+"-1",ChoiceGroup.POPUP,MyCommand.VAR_NAME,null);
		tfCnst1=new TextField(s2[1]+"-1","0",11,TextField.NUMERIC);
		
		//変数・定数２
		cgType2=new ChoiceGroup("TYPE-2"  ,ChoiceGroup.POPUP,s2,null);
		cgVar2=new ChoiceGroup(s2[0]+"-2",ChoiceGroup.POPUP,MyCommand.VAR_NAME,null);
		tfCnst2=new TextField(s2[1]+"-2","0",11,TextField.NUMERIC);

		//ジャンプ
		cgJump=new ChoiceGroup("JUMP TYPE",ChoiceGroup.POPUP,s4,null);
		cgCmpJump=new ChoiceGroup("JUMP TYPE",ChoiceGroup.POPUP,s5,null);
		cgLabel1=new ChoiceGroup(s4[0],ChoiceGroup.POPUP,MyCommand.LBL_NAME,null);
		tfAbsNum=new TextField(s4[1],"1",11,TextField.NUMERIC);
		tfRelNum=new TextField(s4[2],"1",11,TextField.NUMERIC);

		//スクリーン・カラー・オンオフ
		cgScreen=new ChoiceGroup("SCREEN",ChoiceGroup.POPUP,MyCommand.SCR_NAME,null);
		cgColor=new ChoiceGroup("COLOR",ChoiceGroup.POPUP,MyCommand.CLR_NAME,null);
		cgOnOff=new ChoiceGroup(null,ChoiceGroup.POPUP,s3,null);
		
		//メッセージ
		cgMsg=new ChoiceGroup("MESSAGE",ChoiceGroup.POPUP);
		for (i=0;i<MyCommands.MES_SIZE;i++) {
			if (MyCommands.mes[i]!=null) {
				cgMsg.append(Integer.toString(i+1)+":"+MyCommands.mes[i],null);
			} else {
				cgMsg.append(Integer.toString(i+1)+":",null);
			}
		}
		
		//キー
		cgKey=new ChoiceGroup("KEY",ChoiceGroup.POPUP,MyCommand.KEY_NAME,null);
		
		//詳細？
		tfDetail=new TextField(null,null,100,TextField.UNEDITABLE);
		tfDetail.setString(data().strData(Canvas1.detailFlag));

		//アイテムの追加
		append(tfDetail);
		//append(new StringItem(null,"\n"));
		append(tfNum);
		append(cgCmdType);
		mycount=size();

		//コマンドの登録
		addCommand(new Command("OK"    ,Command.OK    ,0));
		addCommand(new Command("Cancel",Command.CANCEL,0));
		
		//リスナーの登録
		setItemStateListener(this);
		setCommandListener(c);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void reset() {
		int i;
		
		cgCmdType.setSelectedIndex(0,true);
		cgCmd1.setSelectedIndex(0,true);
		cgCmd2.setSelectedIndex(0,true);
		cgCmd3.setSelectedIndex(0,true);
		cgCmd4.setSelectedIndex(0,true);
		cgVar0.setSelectedIndex(0,true);
		cgType1.setSelectedIndex(0,true);
		cgVar1.setSelectedIndex(0,true);
		tfCnst1.setString("0");
		cgType2.setSelectedIndex(0,true);
		cgVar2.setSelectedIndex(0,true);
		tfCnst2.setString("0");
		cgJump.setSelectedIndex(0,true);
		cgCmpJump.setSelectedIndex(0,true);
		cgLabel1.setSelectedIndex(0,true);
		tfAbsNum.setString("1");
		tfRelNum.setString("1");
		cgScreen.setSelectedIndex(0,true);
		cgColor.setSelectedIndex(0,true);
		cgOnOff.setSelectedIndex(0,true);
		for (i=0;i<MyCommands.MES_SIZE;i++) {
			if (MyCommands.mes[i]!=null) {
				cgMsg.set(i,Integer.toString(i+1)+":"+MyCommands.mes[i],null);
			} else {
				cgMsg.set(i,Integer.toString(i+1)+":",null);
			}
		}
		cgMsg.setSelectedIndex(0,true);
		cgKey.setSelectedIndex(0,true);
		
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public void reset(int num, String title) {
		setTitle(title);
		tfNum.setConstraints(TextField.UNEDITABLE);
		tfNum.setString(Integer.toString(num));
		reset();
		itemStateChanged(cgCmdType);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public void resetInsert(){
		setTitle("INSERT");
		tfNum.setConstraints(TextField.NUMERIC);
		tfNum.setString("");
		reset();
		itemStateChanged(cgCmdType);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	public void resetRevise(MyCommand mc) {
		setTitle("REVISE");
		tfNum.setConstraints(TextField.UNEDITABLE);
		tfNum.setString(Integer.toString(mc.iNum));
		
		reset();
		
		if (mc.iData[0]<100) {
			cgCmdType.setSelectedIndex(0,true);
			cgCmd1.setSelectedIndex(mc.iData[0],true);
		} else if (mc.iData[0]<200) {
			cgCmdType.setSelectedIndex(1,true);
			cgCmd2.setSelectedIndex(mc.iData[0]%100,true);
		} else if (mc.iData[0]<300) {
			cgCmdType.setSelectedIndex(2,true);
			cgCmd3.setSelectedIndex(mc.iData[0]%100,true);
		} else if (mc.iData[0]<400) {
			cgCmdType.setSelectedIndex(3,true);
			cgCmd4.setSelectedIndex(mc.iData[0]%100,true);
		}

		switch (mc.iData[0]) {
		case MyCommand.CMDNUM_LET:  //CMD (VAR), (VAR/CNST)
		case MyCommand.CMDNUM_RAND:
		case MyCommand.CMDNUM_ADDME:
		case MyCommand.CMDNUM_SUBME:
		case MyCommand.CMDNUM_MULME:
		case MyCommand.CMDNUM_DIVME:
		case MyCommand.CMDNUM_REMME:
		case MyCommand.CMDNUM_SQRT:
		case MyCommand.CMDNUM_RAISEME:
		case MyCommand.CMDNUM_SIN:
		case MyCommand.CMDNUM_COS:
		case MyCommand.CMDNUM_GETARY:
		case MyCommand.CMDNUM_INMAX:
		case MyCommand.CMDNUM_INMIN:
			cgVar0.setSelectedIndex(mc.iData[1],true);
			cgType1.setSelectedIndex(mc.iData[2],true);
			if (mc.iData[2]==0)
				cgVar1.setSelectedIndex(mc.iData[3],true);
			else 
				tfCnst1.setString(Integer.toString(mc.iData[3]));
			break;
		case MyCommand.CMDNUM_ADD:  //CMD (VAR), (VAR/CNST), (VAR/CNST)
		case MyCommand.CMDNUM_SUB:
		case MyCommand.CMDNUM_MUL:
		case MyCommand.CMDNUM_DIV:
		case MyCommand.CMDNUM_REM:
		case MyCommand.CMDNUM_RAISE:
			cgVar0.setSelectedIndex(mc.iData[1],true);
			cgType1.setSelectedIndex(mc.iData[2],true);
			if (mc.iData[2]==0)
				cgVar1.setSelectedIndex(mc.iData[3],true);
			else
				tfCnst1.setString(Integer.toString(mc.iData[3]));
			cgType2.setSelectedIndex(mc.iData[4],true);
			if (mc.iData[4]==0)
				cgVar2.setSelectedIndex(mc.iData[5],true);
			else
				tfCnst2.setString(Integer.toString(mc.iData[5]));
			break;
		case MyCommand.CMDNUM_CMPE:  //CMD (VAR), (VAR/CNST), (LABEL/NUMBER/RETURN)
		case MyCommand.CMDNUM_CMPL:
		case MyCommand.CMDNUM_CMPLE:
		case MyCommand.CMDNUM_CMPS:
		case MyCommand.CMDNUM_CMPSE:
		case MyCommand.CMDNUM_CMPNE:
			cgVar0.setSelectedIndex(mc.iData[1],true);
			cgType2.setSelectedIndex(mc.iData[2],true);
			if (mc.iData[2]==0)
				cgVar2.setSelectedIndex(mc.iData[3],true);
			else
				tfCnst2.setString(Integer.toString(mc.iData[3]));
			cgCmpJump.setSelectedIndex(mc.iData[4],true);
			switch (mc.iData[4]) {
			case 0: case 3:
				cgLabel1.setSelectedIndex(mc.iData[5],true); break;
			case 1: case 4:
				tfAbsNum.setString(Integer.toString(mc.iData[5])); break;
			case 2: case 5:
				tfRelNum.setString(Integer.toString(mc.iData[5])); break;
			}
			break;
		case MyCommand.CMDNUM_LABEL: //CMD (LABEL)
			cgLabel1.setSelectedIndex(mc.iData[1],true);
			break;
		case MyCommand.CMDNUM_GOTO:  //CMD (LABEL/NUMBER)
		case MyCommand.CMDNUM_GOSUB:
			cgJump.setSelectedIndex(mc.iData[1],true);
			if (mc.iData[1]==0)
				cgLabel1.setSelectedIndex(mc.iData[2],true);
			else if (mc.iData[1]==1)
				tfAbsNum.setString(Integer.toString(mc.iData[2]));
			else if (mc.iData[1]==2)
				tfRelNum.setString(Integer.toString(mc.iData[2]));
			break;
		case MyCommand.CMDNUM_PRINT: //CMD (VAR)
		case MyCommand.CMDNUM_INPUT:
		case MyCommand.CMDNUM_POP:
		case MyCommand.CMDNUM_INKEY:
		case MyCommand.CMDNUM_INC:
		case MyCommand.CMDNUM_DEC:
		case MyCommand.CMDNUM_SQRTME:
		case MyCommand.CMDNUM_RVSME:
		case MyCommand.CMDNUM_ADDSF:
		case MyCommand.CMDNUM_MULSF:
			cgVar0.setSelectedIndex(mc.iData[1],true);
			break;
		case MyCommand.CMDNUM_PUSH:  //CMD (VAR/CNST)
		case MyCommand.CMDNUM_WAIT:
		case MyCommand.CMDNUM_POSX:
		case MyCommand.CMDNUM_POSY:
		case MyCommand.CMDNUM_ARCST:
		case MyCommand.CMDNUM_ARCAN:
			cgType1.setSelectedIndex(mc.iData[1],true);
			if (mc.iData[1]==0)
				cgVar1.setSelectedIndex(mc.iData[2],true);
			else
				tfCnst1.setString(Integer.toString(mc.iData[2]));
			break;
		case MyCommand.CMDNUM_ABS:  //CMD (VAR), (VAR)
		case MyCommand.CMDNUM_SGN:
		case MyCommand.CMDNUM_RVS:
		case MyCommand.CMDNUM_SWAP:
			cgVar0.setSelectedIndex(mc.iData[1],true);
			cgVar1.setSelectedIndex(mc.iData[2],true);
			break;
		case MyCommand.CMDNUM_MAX:  //CMD (VAR), (VAR), (VAR/CNST)
		case MyCommand.CMDNUM_MIN:
		case MyCommand.CMDNUM_GCD:
		case MyCommand.CMDNUM_LCM:
			cgVar0.setSelectedIndex(mc.iData[1],true);
			cgVar1.setSelectedIndex(mc.iData[2],true);
			cgType2.setSelectedIndex(mc.iData[3],true);
			if (mc.iData[3]==0)
				cgVar2.setSelectedIndex(mc.iData[4],true);
			else
				tfCnst2.setString(Integer.toString(mc.iData[4]));
			break;
		case MyCommand.CMDNUM_CLS:   //CMD (SCREEN)
			cgScreen.setSelectedIndex(mc.iData[1],true);
			break;
		case MyCommand.CMDNUM_FLUSH: //CMD (ONOFF)
			cgOnOff.setSelectedIndex(mc.iData[1],true);
			break;
		case MyCommand.CMDNUM_COLOR: //CMD (SCREEN), (COLOR)
			cgScreen.setSelectedIndex(mc.iData[1],true);
			cgColor.setSelectedIndex(mc.iData[2],true);
			break;
		case MyCommand.CMDNUM_POSXY:  //CMD (VAR/CNST), (VAR/CNST)
		case MyCommand.CMDNUM_LINE:
		case MyCommand.CMDNUM_RECT:
		case MyCommand.CMDNUM_ARC:
		case MyCommand.CMDNUM_RECTF:
		case MyCommand.CMDNUM_ARCF:
		case MyCommand.CMDNUM_LETARY:
			cgType1.setSelectedIndex(mc.iData[1],true);
			if (mc.iData[1]==0)
				cgVar1.setSelectedIndex(mc.iData[2],true);
			else
				tfCnst1.setString(Integer.toString(mc.iData[2]));
			cgType2.setSelectedIndex(mc.iData[3],true);
			if (mc.iData[3]==0)
				cgVar2.setSelectedIndex(mc.iData[4],true);
			else
				tfCnst2.setString(Integer.toString(mc.iData[4]));
			break;
		case MyCommand.CMDNUM_MESSAGE:  //CMD (MES)
			cgMsg.setSelectedIndex(mc.iData[1],true);
			break;
		case MyCommand.CMDNUM_KEYSTAT:  //CMD (KEYNUM), (LABEL/NUMBER/RETURN)
			cgKey.setSelectedIndex(mc.iData[1],true);
			cgCmpJump.setSelectedIndex(mc.iData[2],true);
			switch (mc.iData[2]) {
			case 0: case 3:
				cgLabel1.setSelectedIndex(mc.iData[3],true); break;
			case 1: case 4:
				tfAbsNum.setString(Integer.toString(mc.iData[3])); break;
			case 2: case 5:
				tfRelNum.setString(Integer.toString(mc.iData[3])); break;
			}
			break;
		}
		
		itemStateChanged(cgCmdType);
	}


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public MyCommand data() {
		int[] d=new int[6];
		
		switch (cgCmdType.getSelectedIndex()){
		case 0:
			d[0]=cgCmd1.getSelectedIndex();
			break;
		case 1:
			d[0]=cgCmd2.getSelectedIndex()+100;
			break;
		case 2:
			d[0]=cgCmd3.getSelectedIndex()+200;
			break;
		case 3:
			d[0]=cgCmd4.getSelectedIndex()+300;
			break;
		}
		
		int vNum,vCnst1,vCnst2,vAbsNum,vRelNum;
		long vl;
		
		if (tfNum.size()==0) {
			vNum=1;
		} else {
			vl=Long.parseLong(tfNum.getString());
			if (vl<(long)Integer.MIN_VALUE) vl=(long)Integer.MIN_VALUE;
			if (vl>(long)Integer.MAX_VALUE) vl=(long)Integer.MAX_VALUE;
			vNum=(int)vl;
		}

		if (tfCnst1.size()==0) { 
			vCnst1=0;
		} else {
			vl=Long.parseLong(tfCnst1.getString());
			if (vl<(long)Integer.MIN_VALUE) vl=(long)Integer.MIN_VALUE;
			if (vl>(long)Integer.MAX_VALUE) vl=(long)Integer.MAX_VALUE;
			vCnst1=(int)vl;
		}

		if (tfCnst2.size()==0) {
			vCnst2=0;
		} else {
			vl=Long.parseLong(tfCnst2.getString());
			if (vl<(long)Integer.MIN_VALUE) vl=(long)Integer.MIN_VALUE;
			if (vl>(long)Integer.MAX_VALUE) vl=(long)Integer.MAX_VALUE;
			vCnst2=(int)vl;
		}

		if (tfAbsNum.size()==0) {
			vAbsNum=1;
		} else {
			vl=Long.parseLong(tfAbsNum.getString());
			if (vl<(long)Integer.MIN_VALUE) vl=(long)Integer.MIN_VALUE;
			if (vl>(long)Integer.MAX_VALUE) vl=(long)Integer.MAX_VALUE;
			vAbsNum=(int)vl;
		}

		if (tfRelNum.size()==0) {
			vRelNum=1;
		} else {
			vl=Long.parseLong(tfRelNum.getString());
			if (vl<(long)Integer.MIN_VALUE) vl=(long)Integer.MIN_VALUE;
			if (vl>(long)Integer.MAX_VALUE) vl=(long)Integer.MAX_VALUE;
			vRelNum=(int)vl;
		}


		switch (d[0]) {
		case MyCommand.CMDNUM_LET:   //CMD (VAR), (VAR/CNST)
		case MyCommand.CMDNUM_RAND:
		case MyCommand.CMDNUM_ADDME:
		case MyCommand.CMDNUM_SUBME:
		case MyCommand.CMDNUM_MULME:
		case MyCommand.CMDNUM_DIVME:
		case MyCommand.CMDNUM_REMME:
		case MyCommand.CMDNUM_SQRT:
		case MyCommand.CMDNUM_RAISEME: 
		case MyCommand.CMDNUM_SIN:
		case MyCommand.CMDNUM_COS:
		case MyCommand.CMDNUM_GETARY:
		case MyCommand.CMDNUM_INMAX:
		case MyCommand.CMDNUM_INMIN:
			d[1]=cgVar0.getSelectedIndex();
			d[2]=cgType1.getSelectedIndex();
			if (d[2]==0)
				d[3]=cgVar1.getSelectedIndex();
			else 
				d[3]=vCnst1;
			break;
		case MyCommand.CMDNUM_ADD:   //CMD (VAR), (VAR/CNST), (VAR/CNST)
		case MyCommand.CMDNUM_SUB:
		case MyCommand.CMDNUM_MUL:
		case MyCommand.CMDNUM_DIV:
		case MyCommand.CMDNUM_REM:
		case MyCommand.CMDNUM_RAISE:
			d[1]=cgVar0.getSelectedIndex();
			d[2]=cgType1.getSelectedIndex();
			if (d[2]==0)
				d[3]=cgVar1.getSelectedIndex();
			else
				d[3]=vCnst1;
			d[4]=cgType2.getSelectedIndex();
			if (d[4]==0)
				d[5]=cgVar2.getSelectedIndex();
			else 
				d[5]=vCnst2;
			break;
		case MyCommand.CMDNUM_CMPE:  //CMD (VAR), (VAR/CNST), (LABEL/NUMBER/RETURN)
		case MyCommand.CMDNUM_CMPL:
		case MyCommand.CMDNUM_CMPLE:
		case MyCommand.CMDNUM_CMPS:
		case MyCommand.CMDNUM_CMPSE:
		case MyCommand.CMDNUM_CMPNE:
			d[1]=cgVar0.getSelectedIndex();
			d[2]=cgType2.getSelectedIndex();
			if (d[2]==0)
				d[3]=cgVar2.getSelectedIndex();
			else 
				d[3]=vCnst2;
			d[4]=cgCmpJump.getSelectedIndex();
			switch (d[4]) {
			case 0: case 3:
				d[5]=cgLabel1.getSelectedIndex(); break;
			case 1: case 4:
				d[5]=vAbsNum; break;
			case 2: case 5:
				d[5]=vRelNum; break;
			}
			break;
		case MyCommand.CMDNUM_LABEL: //CMD (LABEL)
			d[1]=cgLabel1.getSelectedIndex();
			break;
		case MyCommand.CMDNUM_GOTO: //CMD (LABEL/NUMBER)
		case MyCommand.CMDNUM_GOSUB:
			d[1]=cgJump.getSelectedIndex();
			if (d[1]==0)
				d[2]=cgLabel1.getSelectedIndex();
			else if (d[1]==1)
				d[2]=vAbsNum;
			else if (d[1]==2)
				d[2]=vRelNum;
			break;
		case MyCommand.CMDNUM_PRINT: //CMD (VAR)
		case MyCommand.CMDNUM_INPUT:
		case MyCommand.CMDNUM_POP:
		case MyCommand.CMDNUM_INKEY:
		case MyCommand.CMDNUM_INC:
		case MyCommand.CMDNUM_DEC:
		case MyCommand.CMDNUM_SQRTME:
		case MyCommand.CMDNUM_RVSME:
		case MyCommand.CMDNUM_ADDSF:
		case MyCommand.CMDNUM_MULSF:
			d[1]=cgVar0.getSelectedIndex();
			break;
		case MyCommand.CMDNUM_PUSH:  //CMD (VAR/CNST)
		case MyCommand.CMDNUM_WAIT:
		case MyCommand.CMDNUM_POSX:
		case MyCommand.CMDNUM_POSY:
		case MyCommand.CMDNUM_ARCST:
		case MyCommand.CMDNUM_ARCAN:
			d[1]=cgType1.getSelectedIndex();
			if (d[1]==0)
				d[2]=cgVar1.getSelectedIndex();
			else
				d[2]=vCnst1;
			break;
		case MyCommand.CMDNUM_ABS:  //CMD (VAR), (VAR)
		case MyCommand.CMDNUM_SGN:
		case MyCommand.CMDNUM_RVS:
		case MyCommand.CMDNUM_SWAP:
			d[1]=cgVar0.getSelectedIndex();
			d[2]=cgVar1.getSelectedIndex();
			break;
		case MyCommand.CMDNUM_MAX:  //CMD (VAR), (VAR), (VAR/CNST)
		case MyCommand.CMDNUM_MIN:
		case MyCommand.CMDNUM_GCD:
		case MyCommand.CMDNUM_LCM:
			d[1]=cgVar0.getSelectedIndex();
			d[2]=cgVar1.getSelectedIndex();
			d[3]=cgType2.getSelectedIndex();
			if (d[3]==0)
				d[4]=cgVar2.getSelectedIndex();
			else
				d[4]=vCnst2;
			break;
		case MyCommand.CMDNUM_CLS:   //CMD (SCREEN)
			d[1]=cgScreen.getSelectedIndex();
			break;
		case MyCommand.CMDNUM_FLUSH: //CMD (ONOFF)
			d[1]=cgOnOff.getSelectedIndex();
			break;
		case MyCommand.CMDNUM_COLOR: //CMD (SCREEN), (COLOR)
			d[1]=cgScreen.getSelectedIndex();
			d[2]=cgColor.getSelectedIndex();
			break;
		case MyCommand.CMDNUM_POSXY:  //CMD (VAR/CNST), (VAR/CNST)
		case MyCommand.CMDNUM_LINE:
		case MyCommand.CMDNUM_RECT:
		case MyCommand.CMDNUM_ARC:
		case MyCommand.CMDNUM_RECTF:
		case MyCommand.CMDNUM_ARCF:
		case MyCommand.CMDNUM_LETARY:
			d[1]=cgType1.getSelectedIndex();
			if (d[1]==0)
				d[2]=cgVar1.getSelectedIndex();
			else
				d[2]=vCnst1;
			d[3]=cgType2.getSelectedIndex();
			if (d[3]==0)
				d[4]=cgVar2.getSelectedIndex();
			else 
				d[4]=vCnst2;
			break;
		case MyCommand.CMDNUM_MESSAGE:     //CMD (MES)
			d[1]=cgMsg.getSelectedIndex();
			break;
		case MyCommand.CMDNUM_KEYSTAT:  //CMD (KEYNUM), (LABEL/NUMBER/RETURN)
			d[1]=cgKey.getSelectedIndex();
			d[2]=cgCmpJump.getSelectedIndex();
			switch (d[2]) {
			case 0: case 3:
				d[3]=cgLabel1.getSelectedIndex(); break;
			case 1: case 4:
				d[3]=vAbsNum; break;
			case 2: case 5:
				d[3]=vRelNum; break;
			}
			break;
		}

		MyCommand mc=new MyCommand(vNum,d);

		return mc;
	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	public void itemStateChanged(Item item) {
		if ((item==cgType1)||(item==cgType2)||(item==cgCmdType)
					||(item==cgCmd1)||(item==cgCmd2)||(item==cgCmd3)||(item==cgCmd4)
					||(item==cgJump)||(item==cgCmpJump))  {
			int c=0;
			int n=size();
			int i;
			
			for (i=mycount;i<n;i++)
				delete(mycount);
			
			switch (cgCmdType.getSelectedIndex()) {
			case 0:
				c=cgCmd1.getSelectedIndex();
				append(cgCmd1);
				break;
			case 1:
				c=cgCmd2.getSelectedIndex()+100;
				append(cgCmd2);
				break;
			case 2:
				c=cgCmd3.getSelectedIndex()+200;
				append(cgCmd3);
				break;
			case 3:
				c=cgCmd4.getSelectedIndex()+300;
				append(cgCmd4);
				break;
			}
			
			switch (c){
			case MyCommand.CMDNUM_LET:    //CMD (VAR), (VAR/CNST)
			case MyCommand.CMDNUM_RAND:
			case MyCommand.CMDNUM_ADDME:
			case MyCommand.CMDNUM_SUBME:
			case MyCommand.CMDNUM_MULME:
			case MyCommand.CMDNUM_DIVME:
			case MyCommand.CMDNUM_REMME:
			case MyCommand.CMDNUM_SQRT:
			case MyCommand.CMDNUM_SIN:
			case MyCommand.CMDNUM_COS:
			case MyCommand.CMDNUM_RAISEME:
			case MyCommand.CMDNUM_GETARY:
			case MyCommand.CMDNUM_INMAX:
			case MyCommand.CMDNUM_INMIN:
				append(cgVar0);
				append(cgType1);
				if (cgType1.getSelectedIndex()==0)
					append(cgVar1);
				else
					append(tfCnst1);
				break;
			case MyCommand.CMDNUM_ADD:    //CMD (VAR), (VAR/CNST), (VAR/CNST)
			case MyCommand.CMDNUM_SUB:
			case MyCommand.CMDNUM_MUL:
			case MyCommand.CMDNUM_DIV:
			case MyCommand.CMDNUM_REM:
			case MyCommand.CMDNUM_RAISE:
				append(cgVar0);
				append(cgType1);
				if (cgType1.getSelectedIndex()==0)
					append(cgVar1);
				else 
					append(tfCnst1);
				append(cgType2);
				if (cgType2.getSelectedIndex()==0)
					append(cgVar2);
				else
					append(tfCnst2);
				break;
			case MyCommand.CMDNUM_CMPE:   //CMD (VAR), (VAR/CNST), (LABEL/NUMBER/RETURN)
			case MyCommand.CMDNUM_CMPL:
			case MyCommand.CMDNUM_CMPLE:
			case MyCommand.CMDNUM_CMPS:
			case MyCommand.CMDNUM_CMPSE:
			case MyCommand.CMDNUM_CMPNE:
				append(cgVar0);
				append(cgType2);
				if (cgType2.getSelectedIndex()==0)
					append(cgVar2);
				else
					append(tfCnst2);
				append(cgCmpJump);
				switch (cgCmpJump.getSelectedIndex()) {
				case 0: case 3:
					append(cgLabel1); break;
				case 1: case 4:
					append(tfAbsNum); break;
				case 2: case 5:
					append(tfRelNum); break;
				}
				break;
			case MyCommand.CMDNUM_LABEL: //CMD (LABEL)
				append(cgLabel1);
				break;
			case MyCommand.CMDNUM_GOTO:  //CMD (LABEL/NUMBER)
			case MyCommand.CMDNUM_GOSUB:
				append(cgJump);
				if (cgJump.getSelectedIndex()==0)
					append(cgLabel1);
				if (cgJump.getSelectedIndex()==1)
					append(tfAbsNum);
				if (cgJump.getSelectedIndex()==2)
					append(tfRelNum);
				break;
			case MyCommand.CMDNUM_PRINT: //CMD (VAR)
			case MyCommand.CMDNUM_INPUT:
			case MyCommand.CMDNUM_POP:
			case MyCommand.CMDNUM_INKEY:
			case MyCommand.CMDNUM_INC:
			case MyCommand.CMDNUM_DEC:
			case MyCommand.CMDNUM_SQRTME:
			case MyCommand.CMDNUM_RVSME:
			case MyCommand.CMDNUM_ADDSF:
			case MyCommand.CMDNUM_MULSF:
				append(cgVar0);
				break;
			case MyCommand.CMDNUM_PUSH:  //CMD (VAR/CNST)
			case MyCommand.CMDNUM_WAIT:
			case MyCommand.CMDNUM_POSX:
			case MyCommand.CMDNUM_POSY:
			case MyCommand.CMDNUM_ARCST:
			case MyCommand.CMDNUM_ARCAN:
				append(cgType1);
				if (cgType1.getSelectedIndex()==0)
					append(cgVar1);
				else 
					append(tfCnst1);
				break;
			case MyCommand.CMDNUM_ABS:  //CMD (VAR), (VAR)
			case MyCommand.CMDNUM_SGN:
			case MyCommand.CMDNUM_RVS:
			case MyCommand.CMDNUM_SWAP:
				append(cgVar0);
				append(cgVar1);
				break;
			case MyCommand.CMDNUM_MAX:  //CMD (VAR), (VAR), (VAR/CNST)
			case MyCommand.CMDNUM_MIN:
			case MyCommand.CMDNUM_GCD:
			case MyCommand.CMDNUM_LCM:
				append(cgVar0);
				append(cgVar1);
				append(cgType2);
				if (cgType2.getSelectedIndex()==0)
					append(cgVar2);
				else
					append(tfCnst2);
				break;
			case MyCommand.CMDNUM_CLS:    //CMD (SCREEN)
				append(cgScreen);
				break;
			case MyCommand.CMDNUM_FLUSH:  //CMD (ONOFF)
				append(cgOnOff);
				break;
			case MyCommand.CMDNUM_COLOR:  //CMD (SCREEN), (COLOR)
				append(cgScreen);
				append(cgColor);
				break;
			case MyCommand.CMDNUM_POSXY:  //CMD (VAR/CNST), (VAR/CNST)
			case MyCommand.CMDNUM_LINE:
			case MyCommand.CMDNUM_RECT:
			case MyCommand.CMDNUM_ARC:
			case MyCommand.CMDNUM_RECTF:
			case MyCommand.CMDNUM_ARCF:
			case MyCommand.CMDNUM_LETARY:
				append(cgType1);
				if (cgType1.getSelectedIndex()==0)
					append(cgVar1);
				else 
					append(tfCnst1);
				append(cgType2);
				if (cgType2.getSelectedIndex()==0)
					append(cgVar2);
				else
					append(tfCnst2);
				break;
			case MyCommand.CMDNUM_MESSAGE:  //CMD (MES)
				append(cgMsg);
				break;
			case MyCommand.CMDNUM_KEYSTAT:  //CMD (KEYNUM), (LABEL/NUMBER/RETURN)
				append(cgKey);
				append(cgCmpJump);
				switch (cgCmpJump.getSelectedIndex()) {
				case 0: case 3:
					append(cgLabel1); break;
				case 1: case 4:
					append(tfAbsNum); break;
				case 2: case 5:
					append(tfRelNum); break;
				}
				break;
			}
		}
		tfDetail.setString(data().strData(Canvas1.detailFlag));
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //Form2の終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
