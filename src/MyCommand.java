// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MyCommand.java  クラス
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// インポート
//import javax.microedition.lcdui.game.*;
//import javax.microedition.lcdui.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
import java.io.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class MyCommand {

	public static final String[]  SCR_NAME={"ALL", "TEXT", "GRAPHIC"};
	
	public static final String[]  LBL_NAME={"a","b","c","d","e","f","g","h","i","j",
	                                        "k","l","m","n","o","p","q","r","s","t",
	                                        "u","v","w","x","y","z",
	                                        "a'","b'","c'","d'","e'","f'","g'","h'","i'","j'",
	                                        "k'","l'","m'","n'","o'","p'","q'","r'","s'","t'",
	                                        "u'","v'","w'","x'","y'","z'"};                             

	public static final String[]  VAR_NAME={"A","B","C","D","E","F","G","H","I","J",
	                                        "K","L","M","N","O","P","Q","R","S","T",
	                                        "U","V","W","X","Y","Z",
	                                        "A'","B'","C'","D'","E'","F'","G'","H'","I'","J'",
	                                        "K'","L'","M'","N'","O'","P'","Q'","R'","S'","T'",
	                                        "U'","V'","W'","X'","Y'","Z'"};

	public static final String[]  CMD_NAME1={"END"   ,"LABEL"   ,"GOTO"  ,"GOSUB" ,"RETURN",
	                                         "CMPE"  ,"CMPL"    ,"CMPLE" ,"CMPS"  ,"CMPSE" ,
	                                         "CMPNE" ,"INPUT"   ,"PUSH"  ,"POP"   ,"WAIT"  ,
	                                         "RESET" ,"KEYSTAT" ,"INKEY" ,"SWAP"  ,"LETARY",
	                                         "GETARY"};

	public static final String[]  CMD_NAME2={"LET"   ,"ADD"     ,"SUB"   ,"MUL"   ,"DIV",
	                                         "REM"   ,"RAISE"   ,"ABS"   ,"SGN"   ,"RAND",
	                                         "MAX"   ,"MIN"     ,"GCD"   ,"LCM"   };

	public static final String[]  CMD_NAME3={"PRINT" ,"MESSAGE" ,"CLS"   ,"FLUSH" ,"COLOR" ,
	                                         "POSXY" ,"POSX"    ,"POSY"  ,"DOT"   ,"LINE"  ,
	                                         "RECT"  ,"ARCST"   ,"ARCAN" ,"ARC"   ,"RECTF" ,
	                                         "ARCF"  };

	public static final String[]  CMD_NAME4={"INC"   ,"DEC"     ,"ADDME" ,"SUBME"  ,"MULME" ,
	                                         "DIVME" ,"REMME"   ,"SQRT"  ,"SQRTME" ,"RVS"   ,
	                                         "RVSME" ,"ADDSF"   ,"MULSF" ,"RAISEME","SIN"   ,
	                                         "COS"   ,"INMAX"   ,"INMIN" };

	public static final int       CMDNUM_END    =   0, CMDNUM_LABEL   =   1, CMDNUM_GOTO   =   2,
	                              CMDNUM_GOSUB  =   3, CMDNUM_RETURN  =   4, CMDNUM_CMPE   =   5,
	                              CMDNUM_CMPL   =   6, CMDNUM_CMPLE   =   7, CMDNUM_CMPS   =   8,
	                              CMDNUM_CMPSE  =   9, CMDNUM_CMPNE   =  10, CMDNUM_INPUT  =  11,
	                              CMDNUM_PUSH   =  12, CMDNUM_POP     =  13, CMDNUM_WAIT   =  14,
	                              CMDNUM_RESET  =  15, CMDNUM_KEYSTAT =  16, CMDNUM_INKEY  =  17,
	                              CMDNUM_SWAP   =  18, CMDNUM_LETARY  =  19, CMDNUM_GETARY =  20,
	                              CMDNUM_LET    = 100, CMDNUM_ADD     = 101, CMDNUM_SUB    = 102,
	                              CMDNUM_MUL    = 103, CMDNUM_DIV     = 104, CMDNUM_REM    = 105,
	                              CMDNUM_RAISE  = 106, CMDNUM_ABS     = 107, CMDNUM_SGN    = 108,
	                              CMDNUM_RAND   = 109, CMDNUM_MAX     = 110, CMDNUM_MIN    = 111,
	                              CMDNUM_GCD    = 112, CMDNUM_LCM     = 113,
	                              CMDNUM_PRINT  = 200, CMDNUM_MESSAGE = 201, CMDNUM_CLS    = 202,
	                              CMDNUM_FLUSH  = 203, CMDNUM_COLOR   = 204, CMDNUM_POSXY  = 205,
	                              CMDNUM_POSX   = 206, CMDNUM_POSY    = 207, CMDNUM_DOT    = 208,
	                              CMDNUM_LINE   = 209, CMDNUM_RECT    = 210, CMDNUM_ARCST  = 211,
	                              CMDNUM_ARCAN  = 212, CMDNUM_ARC     = 213, CMDNUM_RECTF  = 214,
	                              CMDNUM_ARCF   = 215,
	                              CMDNUM_INC    = 300, CMDNUM_DEC     = 301, CMDNUM_ADDME  = 302,
	                              CMDNUM_SUBME  = 303, CMDNUM_MULME   = 304, CMDNUM_DIVME  = 305,
	                              CMDNUM_REMME  = 306, CMDNUM_SQRT    = 307, CMDNUM_SQRTME = 308,
	                              CMDNUM_RVS    = 309, CMDNUM_RVSME   = 310, CMDNUM_ADDSF  = 311,
	                              CMDNUM_MULSF  = 312, CMDNUM_RAISEME = 313, CMDNUM_SIN    = 314,
	                              CMDNUM_COS    = 315, CMDNUM_INMAX   = 316, CMDNUM_INMIN  = 317;

	public static final String[]  CLR_NAME={"BLACK",   "WHITE", 
	                                        "RED",     "GREEN",  "BLUE",
	                                        "MAGENTA", "YELLOW", "CYAN"};

	public static final int       CLRNUM_BLACK  =  0, CLRNUM_WHITE =  1, CLRNUM_RED     =  2,
	                              CLRNUM_GREEN  =  3, CLRNUM_BLUE  =  4, CLRNUM_MAGENTA =  5,
	                              CLRNUM_YELLOW =  6, CLRNUM_CYAN  =  7;

	public static final String[]  KEY_NAME={"00:NOKEY" ,"01:Num1"  ,"02:Num2"  ,"03:Num3"  ,
	                                        "04:Num4"  ,"05:Num5"  ,"06:Num6"  ,"07:Num7"  , 
	                                        "08:Num8"  ,"09:Num9"  ,"10:Num0"  ,"11:ASTR"  ,
	                                        "12:SHARP" ,"13:UP"    ,"14:DOWN"  ,"15:LEFT"  , 
	                                        "16:RIGHT" ,"17:FIRE"  ,"18:TEL"   ,"19:CLR"   ,
	                                        "20:ANYKEY","21:-Num1" ,"22:-Num2" ,"23:-Num3" ,
	                                        "24:-Num4" ,"25:-Num5" ,"26:-Num6" ,"27:-Num7" ,
	                                        "28:-Num8" ,"29:-Num9" ,"30:-Num0" ,"31:-ASTR" ,
	                                        "32:-SHARP","33:-UP"   ,"34:-DOWN" ,"35:-LEFT" ,
	                                        "36:-RIGHT","37:-FIRE" ,"38:-TEL"  ,"39:-CLR"  };

	public static final int[]     KEY_CODES={      -999,         49,         50,          51,
	                                                 52,         53,         54,          55,         
	                                                 56,         57,         48,          42,         
	                                                 35,         -1,         -2,          -3,         
	                                                 -4,         -5,        -10,          -8};
/*
	public static final int       K_NUM1  =  1, K_NUM2  =  2, K_NUM3  =  3, K_NUM4  =  4,
	                              K_NUM5  =  5, K_NUM6  =  6, K_NUM7  =  7, K_NUM8  =  8,
	                              K_NUM9  =  9, K_NUM0  = 10, K_ASTR  = 11, K_SHARP = 12,
	                              K_UP    = 13, K_DOWN  = 14, K_LEFT  = 15, K_RIGHT = 16,
	                              K_FIRE  = 17, K_TEL   = 18, K_CLR   = 19, K_NOKEY =  0;
*/

	public  int                   iNum=0;
	public  int[]                 iData=new int[6];
	public  MyCommand             next=null, back=null; 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コンストラクタ
	MyCommand(int num, int[] data) {
		int i;
		this.iNum=num;
		for (i=0;i<6;i++)
			this.iData[i]=data[i];
	}
	MyCommand() {
	}
	MyCommand(DataInputStream dis) {
		int i,n;
		
		try {
			iNum=dis.readInt();
			iData[0]=dis.readInt();
			switch (iData[0]) {
			//***********************************************************
			case CMDNUM_LABEL: //1 CMD (LABEL)
			case CMDNUM_PRINT: //1 CMD (VAR)
			case CMDNUM_INPUT:
			case CMDNUM_POP:
			case CMDNUM_INKEY:
			case CMDNUM_INC:
			case CMDNUM_DEC:
			case CMDNUM_SQRTME:
			case CMDNUM_RVSME:
			case CMDNUM_ADDSF:
			case CMDNUM_MULSF:
			case CMDNUM_CLS:   //1 CMD (SCREEN)
			case CMDNUM_FLUSH: //1 CMD (ONOFF)
			case CMDNUM_MESSAGE:  //1 CMD (MES)
				n=1;
				break;
			case CMDNUM_PUSH:  //2 CMD (VAR/CNST)
			case CMDNUM_WAIT:
			case CMDNUM_POSX:
			case CMDNUM_POSY:
			case CMDNUM_ARCST:
			case CMDNUM_ARCAN:
			case CMDNUM_ABS:   //2 CMD (VAR), (VAR)
			case CMDNUM_SGN:
			case CMDNUM_RVS:
			case CMDNUM_SWAP:
			case CMDNUM_COLOR: //2 CMD (SCREEN), (COLOR)
			case CMDNUM_GOTO:  //2 CMD (LABEL/NUMBER)
			case CMDNUM_GOSUB:
				n=2;
				break;
			case CMDNUM_LET:   //3 CMD (VAR), (VAR/CNST)
			case CMDNUM_RAND:
			case CMDNUM_ADDME:
			case CMDNUM_SUBME:
			case CMDNUM_MULME:
			case CMDNUM_DIVME:
			case CMDNUM_REMME:
			case CMDNUM_SQRT:
			case CMDNUM_SIN:
			case CMDNUM_COS:
			case CMDNUM_RAISEME:
			case CMDNUM_GETARY:
			case CMDNUM_INMAX:
			case CMDNUM_INMIN:
			case CMDNUM_KEYSTAT: //3 CMD (KEYNUM), (LABEL/NUMBER/RETURN)
				n=3;
				break;
			case CMDNUM_MAX:   //4 CMD (VAR), (VAR), (VAR/CNST)
			case CMDNUM_MIN:
			case CMDNUM_GCD:
			case CMDNUM_LCM:
			case CMDNUM_POSXY: //4 CMD (VAR/CONST), (VAR/CONST)
			case CMDNUM_LINE:
			case CMDNUM_RECT:
			case CMDNUM_ARC:
			case CMDNUM_RECTF:
			case CMDNUM_ARCF:
			case CMDNUM_LETARY:
				n=4;
				break;
			case CMDNUM_CMPE:  //5 CMD (VAR), (VAR/CNST), (LABEL/NUMBER/RETURN)
			case CMDNUM_CMPL:
			case CMDNUM_CMPLE:
			case CMDNUM_CMPS:
			case CMDNUM_CMPSE:
			case CMDNUM_CMPNE:
			case CMDNUM_ADD:   //5 CMD (VAR), (VAR/CNST), (VAR/CNST)
			case CMDNUM_SUB:
			case CMDNUM_MUL:
			case CMDNUM_DIV:
			case CMDNUM_REM:
			case CMDNUM_RAISE:
				n=5;
				break;
			//**********************************************************
			default:
				n=0;
				break;
			}
			for (i=1;i<=n;i++)
				iData[i]=dis.readInt();
		} catch (Exception e) {
			System.out.println("MyCommand(byte[])::"+e.toString());
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public byte[] toBytes() {
		ByteArrayOutputStream out=new ByteArrayOutputStream(4);
		DataOutputStream      dos=new DataOutputStream(out);
		int i,n;
		byte[] b=null;
		
		try {
			dos.writeInt(iNum);
			dos.writeInt(iData[0]);
			switch (iData[0]) {
			//***********************************************************
			case CMDNUM_LABEL: //1 CMD (LABEL)
			case CMDNUM_PRINT: //1 CMD (VAR)
			case CMDNUM_INPUT:
			case CMDNUM_POP:
			case CMDNUM_INKEY:
			case CMDNUM_INC:
			case CMDNUM_DEC:
			case CMDNUM_SQRTME:
			case CMDNUM_RVSME:
			case CMDNUM_ADDSF:
			case CMDNUM_MULSF:
			case CMDNUM_CLS:   //1 CMD (SCREEN)
			case CMDNUM_FLUSH: //1 CMD (ONOFF)
			case CMDNUM_MESSAGE:  //1CMD (MES)
				n=1;
				break;
			case CMDNUM_PUSH:  //2 CMD (VAR/CNST)
			case CMDNUM_WAIT:
			case CMDNUM_POSX:
			case CMDNUM_POSY:
			case CMDNUM_ARCST:
			case CMDNUM_ARCAN:
			case CMDNUM_ABS:   //2 CMD (VAR), (VAR)
			case CMDNUM_SGN:
			case CMDNUM_RVS:
			case CMDNUM_SWAP:
			case CMDNUM_COLOR: //2 CMD (SCREEN), (COLOR)
			case CMDNUM_GOTO:  //2 CMD (LABEL/NUMBER)
			case CMDNUM_GOSUB:
				n=2;
				break;
			case CMDNUM_LET:   //3 CMD (VAR), (VAR/CNST)
			case CMDNUM_RAND:
			case CMDNUM_ADDME:
			case CMDNUM_SUBME:
			case CMDNUM_MULME:
			case CMDNUM_DIVME:
			case CMDNUM_REMME:
			case CMDNUM_SQRT:
			case CMDNUM_SIN:
			case CMDNUM_COS:
			case CMDNUM_RAISEME:
			case CMDNUM_GETARY:
			case CMDNUM_INMAX:
			case CMDNUM_INMIN:
			case CMDNUM_KEYSTAT: //3 CMD (KEYNUM), (LABEL/NUMBER/RETURN)
				n=3;
				break;
			case CMDNUM_MAX:   //4 CMD (VAR), (VAR), (VAR/CNST)
			case CMDNUM_MIN:
			case CMDNUM_GCD:
			case CMDNUM_LCM:
			case CMDNUM_POSXY: //4 CMD (VAR/CONST), (VAR/CONST)
			case CMDNUM_LINE:
			case CMDNUM_RECT:
			case CMDNUM_ARC:
			case CMDNUM_RECTF:
			case CMDNUM_ARCF:
			case CMDNUM_LETARY:
				n=4;
				break;
			case CMDNUM_CMPE:  //5 CMD (VAR), (VAR/CNST), (LABEL/NUMBER/RETURN)
			case CMDNUM_CMPL:
			case CMDNUM_CMPLE:
			case CMDNUM_CMPS:
			case CMDNUM_CMPSE:
			case CMDNUM_CMPNE:
			case CMDNUM_ADD:   //5 CMD (VAR), (VAR/CNST), (VAR/CNST)
			case CMDNUM_SUB:
			case CMDNUM_MUL:
			case CMDNUM_DIV:
			case CMDNUM_REM:
			case CMDNUM_RAISE:
				n=5;
				break;
			//**********************************************************
			default:
				n=0;
				break;
			}
			for (i=1;i<=n;i++)
				dos.writeInt(iData[i]);
			b=out.toByteArray();
			dos.close();
		} catch (Exception e) {
			System.out.println("MyCommand.toBytes::"+e.toString());
			try {
				dos.close();
			} catch (Exception e2) {
				System.out.println("MyCommand.toBytes(close)::"+e2.toString());
			}
			return null;
		}
		return b;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	public void ReNumber(int newNum) {
		if (newNum!=this.iNum)
			this.iNum=newNum;
		if (this.next!=null)
			this.next.ReNumber(newNum+1);
	}
	
	public void setBack(MyCommand mc) {
		this.back=mc;
		if (this.next!=null)
			this.next.setBack(this);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	public void addnew(MyCommand mc) {
		if (mc.iNum<=this.iNum)
			mc.iNum++;
		if (this.next!=null) {
			this.next.addnew(mc);
		} else {
			this.next=mc;
			mc.back=this;
		}
	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public MyCommand revise(MyCommand mc) {
		if (mc.iNum==this.iNum) {
			mc.next=this.next;
			mc.back=this.back;
			if (mc.next!=null)
				mc.next.back=mc;
			this.next=null;
			this.back=null;
			return mc;
		} else {
			if (this.next!=null) {
				this.next=this.next.revise(mc);
			}
			return this;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	public MyCommand insert(MyCommand mc) {
		if (mc.iNum<this.iNum) {
			mc.next=this;
			mc.back=this.back;
			this.back=mc;
			return mc;
		} else if (mc.iNum==this.iNum) {
			this.iNum++;
			mc.next=this;
			mc.back=this.back;
			this.back=mc;
			if (this.next!=null)
				this.next.ReNumber(this.iNum+1);
			return mc;
		} else {
			if (this.next!=null) {
				this.next=this.next.insert(mc);
			} else {
				this.next=mc;
				mc.back=this;
			}
			return this;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	public MyCommand delNum(int num, boolean renum) {
		MyCommand mc=null;
		if (num==this.iNum) {
			mc=this.next;
			if (mc!=null) {
				mc.back=this.back;
				if (renum)
					mc.ReNumber(this.iNum);
			}
			this.next=null;
			this.back=null;
			return mc;
		} else {
			if (this.next==null) {
				return null;
			} else {
				this.next=this.next.delNum(num,renum);
				return this;
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	public MyCommand del(int cnt, boolean renum) {
		MyCommand mc=null;
		if (cnt==0) {
			mc=this.next;
			if (mc!=null) {
				mc.back=this.back;
				if (renum)
					mc.ReNumber(this.iNum);
			}
			this.next=null;
			this.back=null;
			return mc;
		} else {
			if (this.next!=null)
				this.next=this.next.del(cnt-1,renum);
			return this;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	public int count() {
		if (this.next==null)
			return 1;
		else
			return 1+this.next.count();
	}
	
	public int NumCount(int num) {
		if (num<=this.iNum) {
			return 0;
		} else {
			if (this.next!=null)
				return 1+this.next.NumCount(num);
			else
				return 0;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public MyCommand Data(int cnt) {
		if (cnt==0) {
			return this;
		} else {
			if (this.next!=null) {
				return this.next.Data(cnt-1);
			} else {
				return null;
			}
		}
	}

	// cnt: 相対マイナス値を渡す
	public MyCommand DataRv(int cnt) {
		if (cnt==0) {
			return this;
		} else {
			if (this.back!=null) {
				return this.back.DataRv(cnt+1);
			} else {
				return null;
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public MyCommand DataNum(int num) {
		if (num==this.iNum) {
			return this;
		} else {
			if (this.next!=null) {
				return this.next.DataNum(num);
			} else {
				return null;
			}
		}
	}

	public MyCommand DataNumRv(int num) {
		if (num==this.iNum) {
			return this;
		} else {
			if (this.back!=null) {
				return this.back.DataNumRv(num);
			} else {
				return null;
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public int LastNum() {
		if (this.next==null) 
			return this.iNum;
		else
			return this.next.LastNum();
	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public String strData(boolean dtls) {
		if (dtls==true)
			return strData2();
		else
			return strData();
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public String strData() {
		String s5, s6="", s7="", s8="";
		
		if (iData[0]<100) {
			s5=Integer.toString(iNum)+" "+CMD_NAME1[iData[0]]+" ";
		} else if (iData[0]<200) {
			s5=Integer.toString(iNum)+" "+CMD_NAME2[iData[0]%100]+" ";
		} else if (iData[0]<300) {
			s5=Integer.toString(iNum)+" "+CMD_NAME3[iData[0]%100]+" ";
		} else if (iData[0]<400) {
			s5=Integer.toString(iNum)+" "+CMD_NAME4[iData[0]%100]+" ";
		} else {
			s5="??? ";
		}
		
		
		switch (iData[0]) {
		case CMDNUM_LET:   //CMD (VAR), (VAR/CNST)
		case CMDNUM_RAND:
		case CMDNUM_ADDME:
		case CMDNUM_SUBME:
		case CMDNUM_MULME:
		case CMDNUM_DIVME:
		case CMDNUM_REMME:
		case CMDNUM_SQRT:
		case CMDNUM_SIN:
		case CMDNUM_COS:
		case CMDNUM_RAISEME:
		case CMDNUM_GETARY:
		case CMDNUM_INMAX:
		case CMDNUM_INMIN:
			if (iData[2]==0)
				s6=VAR_NAME[iData[1]]+", "+VAR_NAME[iData[3]];
			else
				s6=VAR_NAME[iData[1]]+", "+Integer.toString(iData[3]);
			break;
		case CMDNUM_ADD:   //CMD (VAR), (VAR/CNST), (VAR/CNST)
		case CMDNUM_SUB:
		case CMDNUM_MUL:
		case CMDNUM_DIV:
		case CMDNUM_REM:
		case CMDNUM_RAISE:
			if (iData[2]==0)
				s7=VAR_NAME[iData[1]]+", "+VAR_NAME[iData[3]]+", ";
			else
				s7=VAR_NAME[iData[1]]+", "+Integer.toString(iData[3])+", ";
			if (iData[4]==0)
				s6=s7+VAR_NAME[iData[5]];
			else
				s6=s7+Integer.toString(iData[5]);
			break;
		case CMDNUM_CMPE:  //CMD (VAR), (VAR/CNST), (LABEL/NUMBER/RETURN)
		case CMDNUM_CMPL:
		case CMDNUM_CMPLE:
		case CMDNUM_CMPS:
		case CMDNUM_CMPSE:
		case CMDNUM_CMPNE:
			if (iData[2]==0)
				s7=VAR_NAME[iData[1]]+", "+VAR_NAME[iData[3]]+", ";
			else
				s7=VAR_NAME[iData[1]]+", "+Integer.toString(iData[3])+", ";
			switch (iData[4]) {
			case 0:
				s6=s7+"GOTO "+LBL_NAME[iData[5]]; break;
			case 1:
				s6=s7+"GOTO ["+Integer.toString(iData[5])+"]"; break;
			case 2:
				if (iData[5]>0) s8="+";
				s6=s7+"GOTO <"+s8+Integer.toString(iData[5])+">"; break;
			case 3:
				s6=s7+"GOSUB "+LBL_NAME[iData[5]]; break;
			case 4:
				s6=s7+"GOSUB ["+Integer.toString(iData[5])+"]"; break;
			case 5:
				if (iData[5]>0) s8="+";
				s6=s7+"GOSUB <"+s8+Integer.toString(iData[5])+">"; break;
			default:
				s6=s7+"RETURN"; break;
			}
			break;
		case CMDNUM_LABEL: //CMD (LABEL)
			s6=LBL_NAME[iData[1]];
			break;
		case CMDNUM_GOTO:  //CMD (LABEL/NUMBER)
		case CMDNUM_GOSUB:
			if (iData[1]==0) {
				s6=LBL_NAME[iData[2]];
			} else if (iData[1]==1) {
				s6="["+Integer.toString(iData[2])+"]";
			} else {
				if (iData[2]>0) s7="+";
				s6="<"+s7+Integer.toString(iData[2])+">";
			}
			break;
		case CMDNUM_PRINT: //CMD (VAR)
		case CMDNUM_INPUT:
		case CMDNUM_POP:
		case CMDNUM_INKEY:
		case CMDNUM_INC:
		case CMDNUM_DEC:
		case CMDNUM_SQRTME:
		case CMDNUM_RVSME:
		case CMDNUM_ADDSF:
		case CMDNUM_MULSF:
			s6=VAR_NAME[iData[1]];
			break;
		case CMDNUM_PUSH:  //CMD (VAR/CNST)
		case CMDNUM_WAIT:
		case CMDNUM_POSX:
		case CMDNUM_POSY:
		case CMDNUM_ARCST:
		case CMDNUM_ARCAN:
			if (iData[1]==0)
				s6=VAR_NAME[iData[2]];
			else
				s6=Integer.toString(iData[2]);
			break;
		case CMDNUM_ABS:   //CMD (VAR), (VAR)
		case CMDNUM_SGN:
		case CMDNUM_RVS:
		case CMDNUM_SWAP:
			s6=VAR_NAME[iData[1]]+", "+VAR_NAME[iData[2]];
			break;
		case CMDNUM_MAX:   //CMD (VAR), (VAR), (VAR/CNST)
		case CMDNUM_MIN:
		case CMDNUM_GCD:
		case CMDNUM_LCM:
			if (iData[3]==0)
				s6=VAR_NAME[iData[1]]+", "+VAR_NAME[iData[2]]+", "+VAR_NAME[iData[4]];
			else
				s6=VAR_NAME[iData[1]]+", "+VAR_NAME[iData[2]]+", "+Integer.toString(iData[4]);
			break;
		case CMDNUM_CLS:   //CMD (SCREEN)
			s6=SCR_NAME[iData[1]];
			break;
		case CMDNUM_FLUSH: //CMD (ONOFF)
			if (iData[1]==0)
				s6="ON";
			else
				s6="OFF";
			break;
		case CMDNUM_COLOR: //CMD (SCREEN), (COLOR)
			s6=SCR_NAME[iData[1]]+", "+CLR_NAME[iData[2]];
			break;
		case CMDNUM_POSXY: //CMD (VAR/CONST), (VAR/CONST)
		case CMDNUM_LINE:
		case CMDNUM_RECT:
		case CMDNUM_ARC:
		case CMDNUM_RECTF:
		case CMDNUM_ARCF:
		case CMDNUM_LETARY:
			if (iData[1]==0)
				s7=VAR_NAME[iData[2]]+", ";
			else
				s7=Integer.toString(iData[2])+", ";
			if (iData[3]==0)
				s6=s7+VAR_NAME[iData[4]];
			else
				s6=s7+Integer.toString(iData[4]);
			break;
		case CMDNUM_MESSAGE:  //CMD (MES)
			if (MyCommands.mes[iData[1]]!=null)
				s6=Integer.toString(iData[1]+1)+":"+MyCommands.mes[iData[1]];
			else
				s6=Integer.toString(iData[1]+1)+":";
			break;
		case CMDNUM_KEYSTAT:  //CMD (KEYNUM), (LABEL/NUMBER/RETURN)
			switch (iData[2]) {
			case 0:
				s6=KEY_NAME[iData[1]]+", GOTO "+LBL_NAME[iData[3]]; break;
			case 1:
				s6=KEY_NAME[iData[1]]+", GOTO ["+Integer.toString(iData[3])+"]"; break;
			case 2:
				if (iData[3]>0) s7="+";
				s6=KEY_NAME[iData[1]]+", GOTO <"+s7+Integer.toString(iData[3])+">"; break;
			case 3:
				s6=KEY_NAME[iData[1]]+", GOSUB "+LBL_NAME[iData[3]]; break;
			case 4:
				s6=KEY_NAME[iData[1]]+", GOSUB ["+Integer.toString(iData[3])+"]"; break;
			case 5:
				if (iData[3]>0) s7="+";
				s6=KEY_NAME[iData[1]]+", GOSUB <"+s7+Integer.toString(iData[3])+">"; break;
			default:
				s6=KEY_NAME[iData[1]]+", RETURN"; break;
			}
			break;
		}
		
		return s5+s6;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public String strData2() {
		String s5, s6="", s7="", s8="", s9="";
		boolean fg=true;
		
		s5=Integer.toString(iNum)+" ";
		
		switch (iData[0]) {
		case CMDNUM_LET:   //CMD (VAR), (VAR/CNST)
			fg=false;
		case CMDNUM_RAND:
			if (fg) { s8="Random("; s9=")"; fg=false;}
		case CMDNUM_ADDME:
			if (fg) { s8=VAR_NAME[iData[1]]+" + ";  fg=false;}
		case CMDNUM_SUBME:
			if (fg) { s8=VAR_NAME[iData[1]]+" - ";  fg=false;}
		case CMDNUM_MULME:
			if (fg) { s8=VAR_NAME[iData[1]]+" * ";  fg=false;}
		case CMDNUM_DIVME:
			if (fg) { s8=VAR_NAME[iData[1]]+" / ";  fg=false;}
		case CMDNUM_REMME:
			if (fg) { s8=VAR_NAME[iData[1]]+" % ";  fg=false;}
		case CMDNUM_SQRT:
			if (fg) { s8="SquareRoot("; s9=")"; fg=false;}
		case CMDNUM_SIN:
			if (fg) { s8="Sin("; s9=")"; fg=false;}
		case CMDNUM_COS:
			if (fg) { s8="Cos("; s9=")"; fg=false;}
		case CMDNUM_RAISEME:
			if (fg) { s8=VAR_NAME[iData[1]]+" ^ ";  fg=false;}
		case CMDNUM_GETARY:
			if (fg) { s8="Array("; s9=")"; fg=false;}
		case CMDNUM_INMAX:
			if (fg) { s8="Max("+VAR_NAME[iData[1]]+", "; s9=")"; fg=false;}
		case CMDNUM_INMIN:
			if (fg) { s8="Min("+VAR_NAME[iData[1]]+", "; s9=")"; fg=false;}
			if (iData[2]==0)
				s6=VAR_NAME[iData[1]]+" := "+s8+VAR_NAME[iData[3]]+s9;
			else
				s6=VAR_NAME[iData[1]]+" := "+s8+Integer.toString(iData[3])+s9;
			break;
		case CMDNUM_ADD:   //CMD (VAR), (VAR/CNST), (VAR/CNST)
			if (fg) { s8=" + "; fg=false;}
		case CMDNUM_SUB:
			if (fg) { s8=" - "; fg=false;}
		case CMDNUM_MUL:
			if (fg) { s8=" * "; fg=false;}
		case CMDNUM_DIV:
			if (fg) { s8=" / "; fg=false;}
		case CMDNUM_REM:
			if (fg) { s8=" % "; fg=false;}
		case CMDNUM_RAISE:
			if (fg) { s8=" ^ "; fg=false;}
			if (iData[2]==0)
				s7=VAR_NAME[iData[1]]+" := "+VAR_NAME[iData[3]]+s8;
			else
				s7=VAR_NAME[iData[1]]+" := "+Integer.toString(iData[3])+s8;
			if (iData[4]==0)
				s6=s7+VAR_NAME[iData[5]];
			else
				s6=s7+Integer.toString(iData[5]);
			break;
		case CMDNUM_CMPE:  //CMD (VAR), (VAR/CNST), (LABEL/NUMBER/RETURN)
			if (fg) { s8=" == "; fg=false;}
		case CMDNUM_CMPL:
			if (fg) { s8=" > "; fg=false;}
		case CMDNUM_CMPLE:
			if (fg) { s8=" >= "; fg=false;}
		case CMDNUM_CMPS:
			if (fg) { s8=" < "; fg=false;}
		case CMDNUM_CMPSE:
			if (fg) { s8=" <= "; fg=false;}
		case CMDNUM_CMPNE:
			if (fg) { s8=" != "; fg=false;}
			if (iData[2]==0)
				s7="If ( "+VAR_NAME[iData[1]]+s8+VAR_NAME[iData[3]]+" ) Then ";
			else
				s7="If ( "+VAR_NAME[iData[1]]+s8+Integer.toString(iData[3])+" ) Then ";
			switch (iData[4]) {
			case 0:
				s6=s7+"Goto "+LBL_NAME[iData[5]]; break;
			case 1:
				s6=s7+"Goto ["+Integer.toString(iData[5])+"]"; break;
			case 2:
				if (iData[5]>0) s8="+";
				s6=s7+"Goto <"+s8+Integer.toString(iData[5])+">"; break;
			case 3:
				s6=s7+"Gosub "+LBL_NAME[iData[5]]; break;
			case 4:
				s6=s7+"Gosub ["+Integer.toString(iData[5])+"]"; break;
			case 5:
				if (iData[5]>0) s8="+";
				s6=s7+"Gosub <"+s8+Integer.toString(iData[5])+">"; break;
			default:
				s6=s7+"Return"; break;
			}
			break;
		case CMDNUM_LABEL: //CMD (LABEL)
			s6="Label "+LBL_NAME[iData[1]];
			break;
		case CMDNUM_GOTO:  //CMD (LABEL/NUMBER)
			if (fg) { s8="Goto "; fg=false;}	
		case CMDNUM_GOSUB:
			if (fg) { s8="Gosub "; fg=false;}
			if (iData[1]==0) {
				s6=s8+LBL_NAME[iData[2]];
			} else if (iData[1]==1) {
				s6=s8+"["+Integer.toString(iData[2])+"]";
			} else {
				if (iData[2]>0) s9="+";
				s6=s8+"<"+s9+Integer.toString(iData[2])+">";
			}
			break;
		case CMDNUM_PRINT: //CMD (VAR)
			if (fg) { s8="Print "; fg=false;}
		case CMDNUM_INPUT:
			if (fg) { s9=" := Input()"; fg=false;}
		case CMDNUM_POP:
			if (fg) { s9=" := Pop()"; fg=false;}
		case CMDNUM_INKEY:
			if (fg) { s9=" := Key()"; fg=false;}
		case CMDNUM_INC:
			if (fg) { s9=" := "+VAR_NAME[iData[1]]+" + 1"; fg=false;}
		case CMDNUM_DEC:
			if (fg) { s9=" := "+VAR_NAME[iData[1]]+" - 1"; fg=false;}
		case CMDNUM_SQRTME:
			if (fg) { s9=" := SquareRoot("+VAR_NAME[iData[1]]+")"; fg=false;}
		case CMDNUM_RVSME:
			if (fg) { s9=" := - "+VAR_NAME[iData[1]]; fg=false;}
		case CMDNUM_ADDSF:
			if (fg) { s9=" := "+VAR_NAME[iData[1]]+" + "+VAR_NAME[iData[1]]; fg=false;}
		case CMDNUM_MULSF:
			if (fg) { s9=" := "+VAR_NAME[iData[1]]+" * "+VAR_NAME[iData[1]]; fg=false;}
			s6=s8+VAR_NAME[iData[1]]+s9;
			break;
		case CMDNUM_PUSH:  //CMD (VAR/CNST)
			if (fg) { s8="Push "; fg=false;}
		case CMDNUM_WAIT:
			if (fg) { s8="Wait "; fg=false;}
		case CMDNUM_POSX:
			if (fg) { s8="Current Position-X := "; fg=false;}
		case CMDNUM_POSY:
			if (fg) { s8="Current Position-Y := "; fg=false;}
		case CMDNUM_ARCST:
			if (fg) { s8="Arc StartAngle "; fg=false;}
		case CMDNUM_ARCAN:
			if (fg) { s8="Arc DrawAngle "; fg=false;}
			if (iData[1]==0)
				s6=s8+VAR_NAME[iData[2]];
			else
				s6=s8+Integer.toString(iData[2]);
			break;
		case CMDNUM_ABS:   //CMD (VAR), (VAR)
			if (fg) { s8="Abosulte("; s9=")"; fg=false;}
		case CMDNUM_SGN:
			if (fg) { s8="Sign("; s9=")"; fg=false;}
		case CMDNUM_RVS:
			if (fg) { s8="- "; fg=false;}
			s6=VAR_NAME[iData[1]]+" := "+s8+VAR_NAME[iData[2]]+s9;
			break;
		case CMDNUM_SWAP:
			s6="Swap("+VAR_NAME[iData[1]]+", "+VAR_NAME[iData[2]]+")";
			break;
		case CMDNUM_MAX:   //CMD (VAR), (VAR), (VAR/CNST)
			if (fg) { s8="Max("; fg=false;}
		case CMDNUM_MIN:
			if (fg) { s8="Min("; fg=false;}
		case CMDNUM_GCD:
			if (fg) { s8="GCD("; fg=false;}
		case CMDNUM_LCM:
			if (fg) { s8="LCM("; fg=false;}
			if (iData[3]==0)
				s6=VAR_NAME[iData[1]]+" := "+s8+VAR_NAME[iData[2]]+", "+VAR_NAME[iData[4]]+")";
			else
				s6=VAR_NAME[iData[1]]+" := "+s8+VAR_NAME[iData[2]]+", "+Integer.toString(iData[4])+")";
			break;
		case CMDNUM_CLS:   //CMD (SCREEN)
			s6="Clear "+SCR_NAME[iData[1]];
			break;
		case CMDNUM_FLUSH: //CMD (ONOFF)
			if (iData[1]==0)
				s6="Graphic Flush ON";
			else
				s6="Graphic Flush OFF";
			break;
		case CMDNUM_COLOR: //CMD (SCREEN), (COLOR)
			s6="Color("+SCR_NAME[iData[1]]+") := "+CLR_NAME[iData[2]];
			break;
		case CMDNUM_POSXY: //CMD (VAR/CONST), (VAR/CONST)
			if (fg) { s8="Current Position-X,Y := "; s9=", "; fg=false;}
		case CMDNUM_LINE:
			if (fg) { s8="Line X2="; s9=", Y2="; fg=false;}
		case CMDNUM_RECT:
			if (fg) { s8="Rectangle width="; s9=", height="; fg=false;}
		case CMDNUM_ARC:
			if (fg) { s8="Arc width="; s9=", height="; fg=false;}
		case CMDNUM_RECTF:
			if (fg) { s8="Rectangle(Fill) width="; s9=", height="; fg=false;}
		case CMDNUM_ARCF:
			if (fg) { s8="Arc(Fill) width="; s9=", height="; fg=false;}
		case CMDNUM_LETARY:
			if (fg) { s8="Array("; s9=") := "; fg=false;}
			if (iData[1]==0)
				s7=s8+VAR_NAME[iData[2]]+s9;
			else
				s7=s8+Integer.toString(iData[2])+s9;
			if (iData[3]==0)
				s6=s7+VAR_NAME[iData[4]];
			else
				s6=s7+Integer.toString(iData[4]);
			break;
		case CMDNUM_MESSAGE:  //CMD (MES)
			if (MyCommands.mes[iData[1]]!=null)
				s6="Message "+Integer.toString(iData[1]+1)+":"+MyCommands.mes[iData[1]];
			else
				s6="Message "+Integer.toString(iData[1]+1)+":";
			break;
		case CMDNUM_KEYSTAT:  //CMD (KEYNUM), (LABEL/NUMBER/RETURN)
			switch (iData[2]) {
			case 0:
				s6="If KeyState("+KEY_NAME[iData[1]]+") Then Goto "+LBL_NAME[iData[3]]; break;
			case 1:
				s6="If KeyState("+KEY_NAME[iData[1]]+") Then Goto ["+Integer.toString(iData[3])+"]"; break;
			case 2:
				if (iData[3]>0) s7="+";
				s6="If KeyState("+KEY_NAME[iData[1]]+") Then Goto <"+s7+Integer.toString(iData[3])+">"; break;
			case 3:
				s6="If KeyState("+KEY_NAME[iData[1]]+") Then Gosub "+LBL_NAME[iData[3]]; break;
			case 4:
				s6="If KeyState("+KEY_NAME[iData[1]]+") Then Gosub ["+Integer.toString(iData[3])+"]"; break;
			case 5:
				if (iData[3]>0) s7="+";
				s6="If KeyState("+KEY_NAME[iData[1]]+") Then Gosub <"+s7+Integer.toString(iData[3])+">"; break;
			default:
				s6="If KeyState("+KEY_NAME[iData[1]]+") Then Return"; break;
			}
			break;
		case CMDNUM_DOT:
			s6="Draw dot"; break;
		case CMDNUM_END:
			s6="End of Program"; break;
		case CMDNUM_RETURN:
			s6="Return"; break;
		case CMDNUM_RESET:
			s6="Reset"; break;
		}
		
		return s5+s6;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //


} //MyCommandの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
