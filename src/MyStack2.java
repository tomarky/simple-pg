// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MyStack2.java  クラス
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// インポート
//import javax.microedition.lcdui.game.*;
//import javax.microedition.lcdui.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
//import java.io.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class MyStack2 {

	iStack stack=null;

	//コンストラクタ
	MyStack2() {
	}
	
	public void reset() {
		while (stack!=null)
			this.pop();
	}
	
	public void push(int d) {
		stack=new iStack(d,stack);
	}
	
	public int pop() throws MyException {
		if (stack==null) {
			throw new MyException();
		}
		
		iStack s=stack;
		stack=s.next;
		int d=s.data;
		s.next=null;
		s=null;
		return d;
	}
	
	class iStack {
		int          data=0;
		iStack       next=null;
	
		iStack(int d, iStack n) {
			data=d;
			next=n;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //MyStack2の終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
