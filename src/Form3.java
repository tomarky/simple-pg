// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// Form3.java  フォーム
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// インポート
import javax.microedition.lcdui.*;
//import javax.microedition.lcdui.game.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
//import java.io.*;
//import java.util.*;


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class Form3 extends Form implements ItemStateListener {

	String[]    sMenu={"ABOUT","MENU","COMMAND1","COMMAND2","COMMAND3","COMMAND4"};
	StringItem  si1=null,si2=null;
	ChoiceGroup cg1=null;
	String      ver=null;
	

	//コンストラクタ
	Form3(CommandListener cl, String v) {
		super("HELP");
		
		ver=new String(v);
		
		addCommand(new Command("Close",Command.BACK,0));
		setCommandListener(cl);
		
		
		cg1=new ChoiceGroup("見たい項目を選べ",ChoiceGroup.POPUP,sMenu,null);
		
		si1=new StringItem("\n",message(0));
		si2=new StringItem("\n",message2(0));
		
		si1.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		si2.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		
		append(cg1);
		append(si1);
		append(si2);
		
		setItemStateListener(this);
	}
	
	private String message(int n) {
		switch (n) {
		case 0:
			return "SimplePG  Version "+ver+"\n\n"
			        +"当アプリは簡素な処理ができるプログラミングアプリです\n"
			        +"アセンブリ言語、行番号つきＢＡＳＩＣをイメージして作りました\n"
			        +"当言語はインタープリタ型で、扱えるのは整数のみです\n"
			        +"当言語は私オリジナルですので慣れないと難しいと思います\n"
			        +"こういうソフトを作るのは初めてですのであまり良い出来ではないです\n"
			        +"編集も不便ですし、デバッグも不便なのです\n"
			        +"そして、残念ながらすごいプログラムは作れません\n"
			        +"さらに、このアプリ自体のデバッグも十分でないため\n"
			        +"私が予想もしなかったエラーが生じるかもしれません\n"
			        +"ですから、あまり期待せずにお使い下さい\n\n"
			        +"本アプリ製作者 Tomarky より";
		case 1:
			return "各メニューの説明です\n\n"
			       +"メインメニュ\n"
			       +" Run    : プログラムを実行します\n"
			       +" Edit   : プログラムの編集をします\n"
			       +" File   : プログラムのセーブやロードをします\n"
			       +" Other  : どうでもいい機能が設置してあります\n"
			       +" Exit   : 本アプリを終了します\n\n"
			       +"Editメニュー\n"
			       +" Back   : メインメニューに戻ります\n"
			       +" AddNew : 最終行に新しいコマンドを追加します\n"
			       +" Revise : 指定行のコマンドを修正します\n"
			       +" Insert : 指定行に新しいコマンドを追加します\n"
			       +" Delete : 指定行のコマンドを削除します\n"
			       +" Test   : 任意のコマンドを実行します\n"
			       +" ListAL : プログラム全行を表示します\n"
			       +" ListAp : 指定行からプログラムを表示します\n"
			       +" SetMsg : プログラムで使用するメッセージを設定します\n\n"
			       +"Fileメニュー\n"
			       +" Back   : メインメニューに戻ります\n"
			       +" Save   : プログラムをセーブします\n"
			       +" Load   : プログラムをロードします\n"
			       +" New    : 編集中のプログラムを破棄します\n\n"
			       +"Otherメニュー\n"
			       +" Back   : メインメニューに戻ります\n"
			       +" Detail : コマンドの表示方法を変更します\n"
			       +" View   : プログラムを表示します\n"
			       +" Help   : ヘルプを開きます\n";
		case 2:
			return "コマンドの説明(1)\n\n"
			       +"大雑把に説明します\n"
			       +"END    : プログラムを終了します\n"
			       +"LABEL  : 行ラベルを設定します\n"
			       +"GOTO   : 指定先にジャンプします\n"
			       +"GOSUB  : 指定先のサブルーチンへジャンプします\n"
			       +"RETURN : サブルーチンから抜け出ます\n"
			       +"CMPE   : 比較対象値が比較値と等しい場合にジャンプ等をします\n"
			       +"CMPL   : 比較対象値が比較値より大きい場合にジャンプ等をします\n"
			       +"CMPLE  : 比較対象値が比較値以上の場合にジャンプ等をします\n"
			       +"CMPS   : 比較対象値が比較値より小さい場合にジャンプ等をします\n"
			       +"CMPSE  : 比較対象値が比較値以下の場合にジャンプ等をします\n"
			       +"CMPNE  : 比較対象値が比較値と異なる場合にジャンプ等をします\n"
			       +"INPUT  : 数値入力を要求します\n"
			       +"PUSH   : スタックへ値を追加します\n"
			       +"POP    : スタックから値を取り出します\n"
			       +"WAIT   : ミリ秒単位で指定した時間を待機します\n"
			       +"RESET  : 変数の値と描写を初期状態に戻します\n"
			       +"KEYSTAT: 指定キーが押されていた場合にジャンプ等をします\n"
			       +"INKEY  : 押されているキーの番号を取得します\n"
			       +"SWAP   : 値を交換します\n"
			       +"LETARY : 配列変数Arrayに値を代入する\n"
			       +"GETARY : 配列変数Arrayから値を取得する\n";

		case 3:
			return "コマンドの説明(2)\n\n"
			       +"大雑把に説明します\n"
			       +"LET    : 変数に値を代入します\n"
			       +"ADD    : ２つの値の和を代入します\n"
			       +"SUB    : ２つの値の差を代入します\n"
			       +"MUL    : ２つの値の積を代入します\n"
			       +"DIV    : ２つの値の商を代入します\n"
			       +"REM    : ２つの値の余りを代入します\n"
			       +"RAISE  : 累乗した値を代入します\n"
			       +"ABS    : 絶対値を代入します\n"
			       +"SGN    : 符号に対応する値を代入します\n"
			       +"RAND   : 乱数を生成して代入します\n"
			       +"MAX    : ２つの値のうち大きいほうを代入します\n"
			       +"MIN    : ２つの値のうち小さいほうを代入します\n"
			       +"GCD    : ２つの値の最大公約数を代入します\n"
			       +"LCM    : ２つの値の最小公倍数を代入します\n";
		case 4:
			return "コマンドの説明(3)\n\n"
			       +"大雑把に説明します\n"
			       +"PRINT  : 変数の値を表示します\n"
			       +"MESSAGE: メッセージを表示します\n"
			       +"CLS    : 表示されてる文字や絵を消します\n"
			       +"FLUSH  : 画面描写のタイミングを調整します\n"
			       +"COLOR  : 描写の色を設定します\n"
			       +"POSXY  : 描写の基準座標を設定します\n"
			       +"POSX   : 描写の基準X座標を設定します\n"
			       +"POSY   : 描写の基準Y座標を設定します\n"
			       +"DOT    : 点を描写します\n"
			       +"LINE   ; 直線を描写します\n"
			       +"RECT   : 長方形を描写します\n"
			       +"ARCST  : 弧の描写開始角度を設定します\n"
			       +"ARCAN  : 弧の描写角度を設定します\n"
			       +"ARC    : 弧を描写します\n"
			       +"RECTF  : 中を塗り潰した長方形を描写します\n"
			       +"ARCF   : 中を塗り潰した扇形を描写します\n";
		case 5:
			return "コマンドの説明(4)\n\n"
			       +"大雑把に説明します\n"
			       +"INC    : インクリメントします\n"
			       +"DEC    : デクリメントします\n"
			       +"ADDME  : 加算代入します\n"
			       +"SUBME  : 減算代入します\n"
			       +"MULME  : 乗算代入します\n"
			       +"DIVME  : 除算代入します\n"
			       +"REMME  : 余り代入します\n"
			       +"SQRT   : 平方根を代入します\n"
			       +"SQRTME : 平方根代入します\n"
			       +"RVS    : 逆符号した値を代入します\n"
			       +"RVSME  : 逆符号代入します\n"
			       +"ADDSF  : ２倍代入します\n"
			       +"MULSF  ; ２乗代入します\n"
			       +"SIN    : １万倍した正弦値を代入します\n"
			       +"COS    : １万倍した余弦値を代入します\n"
			       +"INMAX  : 最大値代入します\n"
			       +"INMIN  : 最小値代入します\n";
		
		default:
			return "";
		}
	}
	
	private String message2(int n) {
		switch (n) {
		case 1:
			return "Run,Test,ListAL,ListAp共通メニュー\n"
			       +" Stop   : 実行中の作業を強制終了します\n\n"
			       +"Saveメニュー\n"
			       +" Back   : Fileメニューに戻ります\n"
			       +" SaveAs : 新規でセーブします\n"
			       +" Save   : 選択中のプログラムに上書きセーブします\n"
			       +" Delete : 選択中のプログラムを削除します\n\n"
			       +"Loadメニュー\n"
			       +" Back   : Fileメニューに戻ります\n"
			       +" Load   : 選択中のプログラムをロードします\n"
			       +" Delete : 選択中のプログラムを削除します\n\n"
			       +"Viewメニュー\n"
			       +" Close  : VIEWを閉じてFileメニューに戻ります\n"
			       +" Next   : プログラムの次の行を表示します\n"
			       +" Back   : プログラムの前の行を表示します\n";
		default:
			return "";
		}
	}
	
	public void itemStateChanged(Item item) {
		if (item==cg1) {
			int n=cg1.getSelectedIndex();
			si1.setText(message(n));
			si2.setText(message2(n));
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //Form3の終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
